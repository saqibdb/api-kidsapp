# KidsApp Backend

KidsApp Python API endpoints for KidsApp.

## Requirement

*  python version 3.6.7

*  certifi version 2018.1.18

*  chardet version 3.0.4

*  Django version 1.11.11

*  django-allauth version 0.35.0

*  django-facebook version 6.0.3

*  django-filter version 1.1.0

*  djangorestframework version 3.7.7

*  idna version 2.6

*  Markdown version 2.6.11

*  oauthlib version 2.0.7

*  python-openid version 2.2.5

*  pytz version 2018.3

*  requests version 2.18.4

*  requests-oauthlib version 0.8.0

*  Unidecode version 1.0.22

*  urllib3 version 1.22

*  whitenoise version 3.2.1

*  dj-database-url version 0.5.0

*  psycopg2 version 2.7.6.1

*  gunicorn version 19.6.0

## Installation

```bash
pip install -r requirements.txt
python manage.py migrate
python manage.py runserver
```

## Usage

```python
^admin/
^log_in/$
^add_school/$
^get_all_schools/$
^add_branch/$
^get_all_branches/$
^get_all_school_branches/$
^add_class/$
^get_all_classes/$
^get_all_branch_classes/$
^add_user/$
^assign_teacher_to_class/$
^assign_student_to_parent/$
^get_all_teachers/$
^get_all_teachers_of_school/$
^get_all_teachers_of_branch/$
^add_post/$
^get_my_created_posts/$
^get_tagged_posts/$
```

## Contributing
No Pull requests are welcomed. For major changes, please open an issue first to discuss what you would like to change.


