from django.conf.urls import url
from django.contrib import admin
from kidApp import views, usersInterface, schoolsInterface, branchesInterface, classesInterface, postsInterface
from django.conf.urls.static import static
from KidsApp.settings import MEDIA_ROOT, MEDIA_URL

from kidApp.push import GCMDeviceViewSet, APNSDeviceViewSet
from django.conf.urls import url



urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^log_in/$', usersInterface.LogIn),
    url(r'^log_in_with_pin/$', usersInterface.LogInWithPin),
    url(r'^add_school/$', schoolsInterface.AddSchool),
    url(r'^get_all_schools/$', schoolsInterface.GetAllSchools),
    url(r'^add_branch/$', branchesInterface.AddBranch),
    url(r'^get_all_branches/$', branchesInterface.GetAllBranches),


    url(r'^edit_branch/$', branchesInterface.EditBranch),
    url(r'^remove_branch/$', branchesInterface.RemoveBranch),
    url(r'^get_detials_of_branch/$', branchesInterface.GetDetialsOfBranch),




    url(r'^get_all_school_branches/$', schoolsInterface.GetAllSchoolBranches),


    url(r'^edit_school/$', schoolsInterface.EditSchool),
    url(r'^remove_school/$', schoolsInterface.RemoveSchool),
    url(r'^get_details_of_school/$', schoolsInterface.GetDetailsOfSchool),





    #url(r'^add_class/$', classesInterface.AddClass),
    url(r'^add_class_with_subjects_ids/$', classesInterface.AddClassWithSubjectsIds),




    url(r'^get_all_classes/$', classesInterface.GetAllClasses),
    url(r'^get_all_branch_classes/$', classesInterface.GetAllBranchClasses),

    url(r'^edit_class/$', classesInterface.EditClass),
    url(r'^remove_class/$', classesInterface.RemoveClass),
    url(r'^get_details_of_class/$', classesInterface.GetDetialsOfClass),







    url(r'^add_user/$', usersInterface.AddUser),
    url(r'^assign_teacher_to_class/$', usersInterface.AssignTeacherToClass),
    url(r'^assign_student_to_class/$', usersInterface.AssignStudentToClass),

    url(r'^add_teacher_with_class/$', usersInterface.AddTeacherWithClass),



    url(r'^get_all_student_of_class/$', usersInterface.GetAllStudentOfClass),
    url(r'^get_all_student/$', usersInterface.GetAllStudents),
    url(r'^get_all_student_branch/$', usersInterface.GetAllStudentsOfBranch),



    url(r'^add_parent_with_students/$', usersInterface.AddParentWithStudents),
    url(r'^edit_teacher/$', usersInterface.EditTeacher),
    url(r'^remove_teacher/$', usersInterface.RemoveTeacher),
    url(r'^get_details_of_teacher/$', usersInterface.GetDetailsOfTeacher),


    url(r'^edit_parent/$', usersInterface.EditParent),
    url(r'^remove_parent/$', usersInterface.RemoveParent),
    url(r'^get_details_of_parent/$', usersInterface.GetDetailsOfParent),







    url(r'^assign_student_to_parent/$', usersInterface.AssignStudentToParent),
    url(r'^get_all_teachers/$', usersInterface.GetAllTeachers),
    url(r'^get_all_teachers_of_school/$', usersInterface.GetAllTeachersOfSchool),
    url(r'^get_all_teachers_of_branch/$', usersInterface.GetAllTeachersOfBranch),
    #url(r'^add_post/$', postsInterface.AddPost),
    #url(r'^add_post_with_media/$', postsInterface.AddPostWithMedia),

    url(r'^add_post_with_media_and_subjects/$', postsInterface.AddPostWithMediaWithSubjects),





    url(r'^add_remove_like_for_post/$', postsInterface.AddRemoveLikeForPost),



    #url(r'^get_my_created_posts/$', postsInterface.GetMyCreatedPosts),
    #url(r'^get_tagged_posts/$', postsInterface.GetTaggedPosts),
    url(r'^add_comment_to_post/$', postsInterface.AddCommentToPost),
    url(r'^set_pin/$', usersInterface.SetPin),
    url(r'^set_password/$', usersInterface.SetPassword),


    url(r'^get_parents_of_student/$', usersInterface.getParentsOfStudent),
    url(r'^get_children_of_parent/$', usersInterface.getChildrenOfParent),
    url(r'^get_all_parents/$', usersInterface.getAllParents),

    url(r'^get_all_admins/$', usersInterface.GetAllAdmins),
    url(r'^get_details_admins/$', usersInterface.GetDetailsOfAdmin),


    url(r'^get_all_teacher_classes/$', classesInterface.GetAllTeacherClasses),

    url(r'^add_subject/$', classesInterface.AddSubject),

    url(r'^get_all_subjects/$', classesInterface.GetAllSubjects),
    url(r'^edit_subject/$', classesInterface.EditSubject),
    url(r'^remove_subject/$', classesInterface.RemoveSubject),
    url(r'^get_details_subject/$', classesInterface.GetDetailsSubject),







    #url(r'^get_all_timeline_posts/$', postsInterface.GetAllTimeLinePosts),

    #url(r'^get_timeline_posts_for_date/$', postsInterface.GetTimeLinePostsForDate),

    url(r'^get_all_timeline_posts_filtered/$', postsInterface.GetAllTimeLinePostsWithFilters),

    url(r'^get_details_of_post/$', postsInterface.GetDetailsOfPost),

    url(r'^upload_file/$', postsInterface.UploadFileToS3),
    url(r'^remove_post_from_timeline/$', postsInterface.RemovePostFromTimeline),
    url(r'^edit_post/$', postsInterface.EditPost),

    url(r'^remove_comment_from_post/$', postsInterface.RemoveCommentFromPost),
    url(r'^edit_comment_from_post/$', postsInterface.EditCommentFromPost),

    url(r'^devices_fcm/$', GCMDeviceViewSet.as_view({'post': 'create'}), name='create_fcm_device'),
    url(r'^devices_apns/$', APNSDeviceViewSet.as_view({'post': 'create'}), name='create_apns_device'),


    url(r'^get_allapns_registrations/$', views.GetAllAPNsRegistrations),
    url(r'^register_device_apn/$', views.RegisterDeviceForAPNs),
    url(r'^un_register_device_apn/$', views.UnRegisterDeviceForAPNs),

    url(r'^register_device_fcm/$', views.RegisterDeviceForFCMs),
    url(r'^un_register_device_fcm/$', views.UnRegisterDeviceForFCMs),


    url(r'^send_apns_all/$', views.SendAPNSToAll),
    url(r'^send_fcm_all/$', views.SendFCMToAll),

    url(r'^enable_disable_Notifications/$', views.EnableDisableNotificationForAccount),



] + static(MEDIA_URL, document_root=MEDIA_ROOT)
