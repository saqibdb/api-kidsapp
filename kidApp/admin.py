from django.contrib import admin
from kidApp.models import User, School, Branch, Class, TeacherClass, StudentClass,SubjectClass ,SubjectPost , StudentParent, Post, Comment, Like, Media, Tag, Subject
from import_export.admin import ImportExportMixin, ExportActionModelAdmin

# Register your models here.

class UserAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'userName',
        'email',
        'creationDate',
        'lastLoginDate',
        'authToken',
        'type',
        'isPasswordPinChangeNeeded',
        'isActive'
    )
    search_fields = ('userName', 'email')
admin.site.register(User, UserAdmin)

class SchoolAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'schoolName',
        'schoolAddress',
        'creationDate',
        'updationDate',
        'isActive'
    )
    search_fields = ('schoolName', 'schoolAddress')

admin.site.register(School, SchoolAdmin)


class BranchAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'branchName',
        'branchAddress',
        'creationDate',
        'updationDate',
        'isActive'
    )
    search_fields = ('schoolName', 'schoolAddress')

admin.site.register(Branch, BranchAdmin)

class ClassAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'className',
        'creationDate',
        'updationDate',
        'isActive'
    )
    search_fields = ('schoolName', 'schoolAddress')

admin.site.register(Class, ClassAdmin)

class TeacherClassAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'classId',
        'isClassTeacher',
        'teacherId',
    )
    search_fields = ('classId', 'teacherId')
admin.site.register(TeacherClass, TeacherClassAdmin)


class StudentClassAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'classId',
        'studentId',
    )
    search_fields = ('classId', 'studentId')
admin.site.register(StudentClass, StudentClassAdmin)

class SubjectClassAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'classId',
        'subjectId',
    )
    search_fields = ('classId', 'subjectId')
admin.site.register(SubjectClass, SubjectClassAdmin)


class SubjectPostAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'postId',
        'subjectId',
    )
    search_fields = ('classId', 'postId')
admin.site.register(SubjectPost, SubjectPostAdmin)






class StudentParentAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'studentId',
        'parentId',
    )
    search_fields = ('studentId', 'parentId')
admin.site.register(StudentParent, StudentParentAdmin)

class PostAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'postTitle',
        'postDescription',
        'updationDate',
        'creatorId',
        'isActive'
    )
    search_fields = ('postTitle', 'creatorId')
admin.site.register(Post, PostAdmin)

admin.site.register(Comment)
admin.site.register(Like)



class MediaAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'mediaLink',
        'mediaType',
        'creationDate',
        'postId'
    )
    search_fields = ('mediaType', 'postId')
admin.site.register(Media, MediaAdmin)

class TagAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'postId',
        'userId',
        'creationDate'
    )
    search_fields = ('postId', 'userId')
admin.site.register(Tag, TagAdmin)


class SubjectAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'subjectName',
        'isActive',
        'creationDate',
        'updationDate'
    )
    search_fields = ('id','subjectName')
admin.site.register(Subject, SubjectAdmin)
