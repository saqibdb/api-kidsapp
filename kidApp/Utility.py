from rest_framework import status
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from kidApp.models import User, School, Branch, Class, TeacherClass, StudentClass, StudentParent, Post, Comment, Like, Media, Tag
from kidApp import Utility, Constants
import boto
from boto.s3.connection import S3Connection
from boto.s3.key import Key

class Utility(object):

    def __init__(self, get_response):
        self.get_response = get_response


def make_AWS_Connection():
    return boto.s3.connect_to_region(Constants.AWS_REGION(),
                              aws_access_key_id=Constants.AWS_ACCESS_KEY(),
                              aws_secret_access_key=Constants.AWS_SECRET_KEY(),
                              # is_secure=True,               # uncomment if you are not using ssl
                              calling_format=boto.s3.connection.OrdinaryCallingFormat(),
                              )