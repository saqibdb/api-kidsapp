import base64
from rest_framework.response import Response
from rest_framework.decorators import api_view
from kidApp.models import User, School, Branch, Class, TeacherClass, StudentClass, StudentParent, Post, Comment, Like, Media, Tag
import uuid
import random
import json
from KidsApp.settings import MEDIA_URL, MEDIA_ROOT
from kidApp import Utility, Constants


import boto
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import base64
import time
from django.db.models import Q

@api_view(['POST', ])
def LogIn(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            userName = str(body['user_name']).lower()
            password = body['password']
            allFoundUsers = User.objects.filter(userName=str(userName),isActive=True)
            if allFoundUsers.count() == 0:
                return Response({"status" : "No User Found"}AKIAJBOEQASD4L7YUKFQ)
            newAuthToken = uuid.uuid4()
            for j in allFoundUsers:
                if not j.authToken:
                    j.authToken = newAuthToken
                    j.save()


            users = User.objects.filter(userName=str(userName),isActive=True).values('id',
                                                            'userName',
                                                            'email',
                                                            'type',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'lastLoginDate',
                                                            'authToken',
                                                            'isPasswordPinChangeNeeded',
                                                            'isNotification',
                                                            'password')
            user = list(users)[0]
            if user['password'] == str(password):
                    del user['password']
                    return Response({"User" :user, "status" : "User Found"})
            else:
                    return Response({"status" : "Password is not correct"})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def LogInWithPin(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            userName = str(body['user_name']).lower()
            pin = body['pin']
            allFoundUsers = User.objects.filter(userName=str(userName),pin=str(pin),isActive=True)
            if allFoundUsers.count() == 0:
                return Response({"status" : "Pin is not correct"})
            newAuthToken = uuid.uuid4()
            for j in allFoundUsers:
                if not j.authToken:
                    j.authToken = newAuthToken
                    j.save()

            users = User.objects.filter(userName=str(userName),isActive=True).values('id',
                                                            'userName',
                                                            'email',
                                                            'type',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'lastLoginDate',
                                                            'authToken',
                                                            'isPasswordPinChangeNeeded',
                                                            'isNotification')
            user = list(users)[0]
            return Response({"User" :user, "status" : "User Found"})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def SetPin(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            pin = body['pin']
            users = User.objects.filter(authToken=str(auth),isActive=True)
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct. Please Login Again."})
            user = list(users)[0]
            user.pin = pin
            user.isPasswordPinChangeNeeded = False
            user.save()
            users = User.objects.filter(id=user.id).values('id',
                                                            'userName',
                                                            'email',
                                                            'type',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'lastLoginDate',
                                                            'authToken',
                                                            'isPasswordPinChangeNeeded',
                                                            'isNotification')
            user = list(users)[0]
            return Response({"User" :user, "Status" :"Pin has been made."})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def SetPassword(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            password = body['password']
            users = User.objects.filter(authToken=str(auth),isActive=True)
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct. Please Login Again."})
            user = list(users)[0]
            user.password = password
            user.save()

            return Response({"Status" :"Password has been changed."})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def AddUser(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            userName = str(body['user_name']).lower()
            email = str(body['email']).lower()
            password = body['password']
            age = body['age']
            phone = body['phone']
            gender = body['gender']
            fullName = body['full_name']
            isNotification = body['is_notification']
            profilePictureURL = ""
            profilePictureDATA = body['profile_picture_data']


            deviceToken = body['device_token']
            type = body['type']
            studentIdentification = body['student_identification']
            branchId = 0
            if body['type'] == "SchoolAdmin":
                type = "S-Admin"
                if 'school_id' in body:
                    branchId = int(body['school_id'])
            elif body['type'] == "BranchAdmin":
                type = "B-Admin"
                if 'branch_id' in body:
                    branchId = int(body['branch_id'])

            users = User.objects.filter(authToken=str(auth)).values('type','id')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                
            alreadyUsers = User.objects.filter(userName=str(userName))
            if alreadyUsers.count() > 0:
                return Response({"Error" :"There is already a User present with this Username"})



            mediaData = profilePictureDATA
            extension = ".jpg"
            contentType = "image/jpeg"
            connection = Utility.make_AWS_Connection()
            bucket = connection.get_bucket(Constants.AWS_BUCKET_NAME())
            cloudfile = Key(bucket)
            millis = int(round(time.time() * 1000))
            path = "KidsApp/" + str(user['id']) + str(millis) + ".jpg"
            cloudfile.key = path
            cloudfile.set_contents_from_string(base64.b64decode(mediaData))
            cloudfile.set_metadata('Content-Type', contentType)
            cloudfile.set_acl('public-read')
            profilePictureURL = Constants.AWS_BASE_URL() + path


            new = User(userName=str(userName),
                    email=str(email),
                    password=str(password),
                    age=str(age),
                    phone=str(phone),
                    gender=str(gender),
                    isNotification=isNotification,
                    profilePictureURL=str(profilePictureURL),
                    deviceToken=str(deviceToken),
                    type=str(type),
                    studentIdentification=str(studentIdentification),
                    isPasswordPinChangeNeeded=True,
                    fullName=fullName,
                    branchId=int(branchId))
            new.save()
            users = User.objects.filter(id=new.id).values('id',
                                                            'userName',
                                                            'email',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'isNotification',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'type',
                                                            'studentIdentification',
                                                            'creationDate',
                                                            'updationDate',
                                                            'lastLoginDate',
                                                            'isPasswordPinChangeNeeded',
                                                            'fullName',
                                                            'branchId')
            finalUser = list(users)[0]
            if finalUser['type'] == "S-Admin":
                finalUser['type'] = "SchoolAdmin"
                schools = School.objects.filter(id=int(finalUser['branchId'])).values('id',
                                                'schoolName',
                                                'schoolAddress',
                                                'creationDate',
                                                'updationDate')
                if schools.count() > 0:
                    finalUser['School'] = list(schools)[0]

            if finalUser['type'] == "B-Admin":
                finalUser['type'] = "BranchAdmin"

                branches = Branch.objects.filter(id=int(finalUser['branchId']),isActive=True).values('id',
                                                'branchName',
                                                'branchAddress',
                                                'creationDate',
                                                'updationDate',
                                                'schoolId')
                if branches.count() > 0:
                    branch = list(branches)[0]
                    schools = School.objects.filter(id=int(branch['schoolId'])).values('id',
                                                    'schoolName',
                                                    'schoolAddress',
                                                    'creationDate',
                                                    'updationDate')
                    if schools.count() > 0:
                        branch['School'] = list(schools)[0]
                    finalUser['Branch'] = branch



            return Response({"NewUser" :finalUser})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def AssignTeacherToClass(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            classId = body['class_id']
            teacherId = body['teacher_id']

            isClassTeacher = False
            if 'is_class_teacher' in body:
                isClassTeacher = body['is_class_teacher']

            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] == "Teacher" or user['type'] == "Parent":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to assign teacher to class."})
            teacherUsers = User.objects.filter(id=int(teacherId),isActive=True).values('type')
            if teacherUsers.count() == 0:
                return Response({"Error" :"No User Found With the specified id."})
            else:
                teacherUser = list(teacherUsers)[0]
                if teacherUser['type'] != "Teacher":
                    return Response({"Error" :"Specified User is not Teacher."})
            teacherClasses = TeacherClass.objects.filter(classId=int(classId),teacherId=int(teacherId))
            if teacherClasses.count() > 0:
                return Response({"Error" :"This Teacher is already assigned to this Class"})

            new = TeacherClass(classId=int(classId),teacherId=int(teacherId),isClassTeacher=isClassTeacher)
            new.save()
            classes = Class.objects.filter(id=int(classId)).values('id',
                                                            'className',
                                                            'creationDate',
                                                            'updationDate')
            classe = list(classes)[0]
            allClassTeachers = []

            teacherClasses = TeacherClass.objects.filter(classId=int(classe['id'])).values('id',
                                                            'classId',
                                                            'teacherId',
                                                            'isClassTeacher')
            for teacherClasse in teacherClasses:
                users = User.objects.filter(id=int(teacherClasse['teacherId']),isActive=True).values('id',
                                                                'userName',
                                                                'email',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'isNotification',
                                                                'profilePictureURL',
                                                                'deviceToken',
                                                                'type',
                                                                'studentIdentification',
                                                                'creationDate',
                                                                'updationDate',
                                                                'lastLoginDate')
                for user in users:
                    user['isClassTeacher'] = teacherClasse['isClassTeacher']
                if users.count() > 0:
                    allClassTeachers.append(list(users)[0])

            classe['teachers'] = allClassTeachers
            return Response({"Class" :classe})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def AssignStudentToClass(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            classId = body['class_id']
            studentsArray = body['students']
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] == "Teacher" or user['type'] == "Parent":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to assign student to class."})

            for studentObj in studentsArray:
                studentId = studentObj['student_id']
                studentUsers = User.objects.filter(id=int(studentId),isActive=True).values('type')
                if studentUsers.count() == 0:
                    return Response({"Error" :"No User Found With the specified id."})
                else:
                    studentUser = list(studentUsers)[0]
                    if studentUser['type'] != "Student":
                        return Response({"Error" :"Specified User is not Student."})
                    else:
                        studentClasses = StudentClass.objects.filter(classId=int(classId),studentId=int(studentId))
                        if studentClasses.count() == 0:
                            new = StudentClass(classId=int(classId),studentId=int(studentId))
                            new.save()
            classes = Class.objects.filter(id=int(classId)).values('id',
                                                            'className',
                                                            'creationDate',
                                                            'updationDate')
            classe = list(classes)[0]
            allClassStudents = []
            studentClasses = StudentClass.objects.filter(classId=int(classe['id'])).values('id',
                                                            'classId',
                                                            'studentId')
            for studentClasse in studentClasses:
                users = User.objects.filter(id=int(studentClasse['studentId']),isActive=True).values('id',
                                                                'userName',
                                                                'email',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'isNotification',
                                                                'profilePictureURL',
                                                                'deviceToken',
                                                                'type',
                                                                'studentIdentification',
                                                                'creationDate',
                                                                'updationDate',
                                                                'lastLoginDate')
                if users.count() > 0:
                    allClassStudents.append(list(users)[0])
            classe['students'] = allClassStudents
            return Response({"Class" :classe})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})






@api_view(['POST', ])
def AddTeacherWithClass(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            userName = str(body['user_name']).lower()
            email = str(body['email']).lower()
            password = body['password']
            age = body['age']
            phone = body['phone']
            gender = body['gender']
            fullName = body['full_name']
            isNotification = body['is_notification']
            profilePictureURL = ""
            profilePictureDATA = body['profile_picture_data']
            deviceToken = ""
            type = "Teacher"
            studentIdentification = ""
            branchId = body['branch_id']
            isClassTeacher = False

            assignedClasses = []
            if 'assigned_classes' in body:
                assignedClasses = body['assigned_classes']

            users = User.objects.filter(authToken=str(auth)).values('type','id')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if type == "Student":
                    if user['type'] == "Teacher" or user['type'] == "Parent":
                        return Response({"Error" :"User of type "+user['type']+" do not have ability to add new Student."})

                if type == "Teacher":
                    if user['type'] == "Teacher" or user['type'] == "Student" or user['type'] == "Parent":
                        return Response({"Error" :"User of type "+user['type']+" do not have ability to add new Teacher."})

                if type == "Parent":
                    if user['type'] == "Teacher" or user['type'] == "Student" or user['type'] == "Parent":
                        return Response({"Error" :"User of type "+user['type']+" do not have ability to add new Parent."})

                if type == "BranchAdmin":
                    if user['type'] == "Teacher" or user['type'] == "Student" or user['type'] == "BranchAdmin" or user['type'] == "Parent":
                        return Response({"Error" :"User of type "+user['type']+" do not have ability to add new BranchAdmin."})

                if type == "SchoolAdmin":
                    if user['type'] == "Teacher" or user['type'] == "Student" or user['type'] == "BranchAdmin" or user['type'] == "SchoolAdmin" or user['type'] == "Parent":
                        return Response({"Error" :"User of type "+user['type']+" do not have ability to add new SchoolAdmin."})

                if type == "SuperAdmin":
                    if user['type'] == "Teacher" or user['type'] == "Student" or user['type'] == "BranchAdmin" or user['type'] == "SchoolAdmin" or user['type'] == "Parent":
                        return Response({"Error" :"User of type "+user['type']+" do not have ability to add new SuperAdmin."})

            alreadyUsers = User.objects.filter(userName=str(userName),isActive=True)
            if alreadyUsers.count() > 0:
                return Response({"Error" :"There is already a User present with this Username"})

            mediaData = profilePictureDATA
            extension = ".jpg"
            contentType = "image/jpeg"
            connection = Utility.make_AWS_Connection()
            bucket = connection.get_bucket(Constants.AWS_BUCKET_NAME())
            cloudfile = Key(bucket)
            millis = int(round(time.time() * 1000))
            path = "KidsApp/" + str(user['id']) + str(millis) + ".jpg"
            cloudfile.key = path
            cloudfile.set_contents_from_string(base64.b64decode(mediaData))
            cloudfile.set_metadata('Content-Type', contentType)
            cloudfile.set_acl('public-read')
            profilePictureURL = Constants.AWS_BASE_URL() + path
            new = User(userName=str(userName),
                    email=str(email),
                    password=str(password),
                    age=str(age),
                    phone=str(phone),
                    gender=str(gender),
                    isNotification=isNotification,
                    profilePictureURL=str(profilePictureURL),
                    deviceToken=str(deviceToken),
                    type=str(type),
                    studentIdentification=str(studentIdentification),
                    isPasswordPinChangeNeeded=True,
                    branchId=int(branchId),
                    fullName=fullName)
            new.save()
            for assignedClass in assignedClasses:
                assignedClassId = assignedClass['class_id']
                assignedClassTeacher = assignedClass['is_class_teacher']
                newTeacherClass = TeacherClass(classId=int(assignedClassId),teacherId=int(new.id),isClassTeacher=bool(assignedClassTeacher))
                newTeacherClass.save()


            teacherUsers = User.objects.filter(id=new.id,isActive=True).values('id',
                                                            'userName',
                                                            'email',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'isNotification',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'type',
                                                            'studentIdentification',
                                                            'creationDate',
                                                            'updationDate',
                                                            'lastLoginDate',
                                                            'isPasswordPinChangeNeeded',
                                                            'branchId',
                                                            'fullName')



            teacher = list(teacherUsers)[0]
            allTeacherClasses = []

            teacherClasses = TeacherClass.objects.filter(teacherId=int(teacher['id'])).values('id',
                                                            'classId',
                                                            'teacherId',
                                                            'isClassTeacher')
            for teacherClasse in teacherClasses:
                classes = Class.objects.filter(id=int(teacherClasse['classId'])).values('id',
                                                                'className',
                                                                'creationDate',
                                                                'updationDate')

                for classe in classes:
                    classe['isClassTeacher'] = teacherClasse['isClassTeacher']
                allTeacherClasses.extend(classes)
            teacher['classes'] = allTeacherClasses
            return Response({"NewTeacher" :teacher})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})











@api_view(['POST', ])
def GetAllStudentOfClass(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            classId = body['class_id']

            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                
            if user['type'] != "Parent":
                allClassStudents = []
                studentClasses = StudentClass.objects.filter(classId=int(classId)).values('id',
                                                                'classId',
                                                                'studentId')
                for studentClasse in studentClasses:
                    users = User.objects.filter(id=int(studentClasse['studentId']),isActive=True).values('id',
                                                                    'userName',
                                                                    'email',
                                                                    'age',
                                                                    'phone',
                                                                    'gender',
                                                                    'isNotification',
                                                                    'profilePictureURL',
                                                                    'deviceToken',
                                                                    'type',
                                                                    'studentIdentification',
                                                                    'creationDate',
                                                                    'updationDate',
                                                                    'lastLoginDate')
                    if users.count() > 0:
                        allClassStudents.append(list(users)[0])
                allClassStudents = sorted(allClassStudents, key=lambda x: x['updationDate'], reverse=True)
                return Response({"Students" :list(allClassStudents)})
            else:
                allClassTeachers = []
                teacherClasses = TeacherClass.objects.filter(classId=int(classId)).values('id',
                                                                                            'classId',
                                                                                            'teacherId',
                                                                                            'isClassTeacher')
                for teacherClass in teacherClasses:
                    users = User.objects.filter(id=int(teacherClass['teacherId']),isActive=True).values('id',
                                                                                                    'userName',
                                                                                                    'email',
                                                                                                    'age',
                                                                                                    'phone',
                                                                                                    'gender',
                                                                                                    'isNotification',
                                                                                                    'profilePictureURL',
                                                                                                    'deviceToken',
                                                                                                    'type',
                                                                                                    'studentIdentification',
                                                                                                    'creationDate',
                                                                                                    'updationDate',
                                                                                                    'lastLoginDate')
            
            
                    if users.count() > 0:
                            allClassTeachers.append(list(users)[0])
                allClassTeachers = sorted(allClassTeachers, key=lambda x: x['updationDate'], reverse=True)
                return Response({"Students" :list(allClassTeachers)})
            
            
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})








@api_view(['POST', ])
def AssignStudentToParent(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            parentId = body['parent_id']
            studentId = body['student_id']
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] == "Teacher" or user['type'] == "Parent":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to assign student to parent."})
            studentUsers = User.objects.filter(id=int(studentId),isActive=True).values('type')
            if studentUsers.count() == 0:
                return Response({"Error" :"No User Found With the specified id."})
            else:
                studentUser = list(studentUsers)[0]
                if studentUser['type'] != "Student":
                    return Response({"Error" :"Specified User is not Student."})
            studentParents = StudentParent.objects.filter(studentId=int(studentId),parentId=int(parentId))
            if studentParents.count() > 0:
                return Response({"Error" :"This Student is already assigned to this Parent"})

            new = StudentParent(studentId=int(studentId),parentId=int(parentId))
            new.save()
            parents = User.objects.filter(id=int(parentId),isActive=True).values('id',
                                                            'userName',
                                                            'email',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'isNotification',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'type',
                                                            'studentIdentification',
                                                            'creationDate',
                                                            'updationDate',
                                                            'lastLoginDate')
            parent = list(parents)[0]
            allParentStudents = []

            studentParents = StudentParent.objects.filter(parentId=int(parent['id'])).values('id',
                                                            'studentId',
                                                            'parentId')
            for studentParent in studentParents:
                users = User.objects.filter(id=int(studentParent['studentId']),isActive=True).values('id',
                                                                'userName',
                                                                'email',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'isNotification',
                                                                'profilePictureURL',
                                                                'deviceToken',
                                                                'type',
                                                                'studentIdentification',
                                                                'creationDate',
                                                                'updationDate',
                                                                'lastLoginDate')
                if users.count() > 0:
                    allParentStudents.append(list(users)[0])

            parent['students'] = allParentStudents
            return Response({"Parent" :parent})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def GetAllTeachers(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            page = int(body['page'])
            itemsPerPage = int(body['per_page'])
            users = User.objects.filter(authToken=str(auth)).values('type','branchId')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]

            users = User.objects.filter(type="Teacher",isActive=True).values('id',
                                                            'userName',
                                                            'email',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'isNotification',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'type',
                                                            'studentIdentification',
                                                            'creationDate',
                                                            'updationDate',
                                                            'lastLoginDate',
                                                            'branchId',
                                                            'fullName')

            if user['type'] == "S-Admin":
                users = []
                schools = School.objects.filter(id=int(user['branchId'])).values('id',
                                                'schoolName',
                                                'schoolAddress',
                                                'creationDate',
                                                'updationDate')
                for school in schools:
                    branches = Branch.objects.filter(schoolId=int(school['id'])).values('id',
                                                    'branchName',
                                                    'branchAddress',
                                                    'creationDate',
                                                    'updationDate',
                                                    'schoolId')
                    for branch in branches:
                        branchUsers = User.objects.filter(type="Teacher",branchId=int(branch['id']),isActive=True).values('id',
                                                                        'userName',
                                                                        'email',
                                                                        'age',
                                                                        'phone',
                                                                        'gender',
                                                                        'isNotification',
                                                                        'profilePictureURL',
                                                                        'deviceToken',
                                                                        'type',
                                                                        'studentIdentification',
                                                                        'creationDate',
                                                                        'updationDate',
                                                                        'lastLoginDate',
                                                                        'branchId',
                                                                        'fullName')
                        users.extend(branchUsers)


            if user['type'] == "B-Admin":
                users = []
                branches = Branch.objects.filter(id=int(user['branchId'])).values('id',
                                                'branchName',
                                                'branchAddress',
                                                'creationDate',
                                                'updationDate',
                                                'schoolId')
                for branch in branches:
                    branchUsers = User.objects.filter(type="Teacher",branchId=int(branch['id']),isActive=True).values('id',
                                                                    'userName',
                                                                    'email',
                                                                    'age',
                                                                    'phone',
                                                                    'gender',
                                                                    'isNotification',
                                                                    'profilePictureURL',
                                                                    'deviceToken',
                                                                    'type',
                                                                    'studentIdentification',
                                                                    'creationDate',
                                                                    'updationDate',
                                                                    'lastLoginDate',
                                                                    'branchId',
                                                                    'fullName')
                    users.extend(branchUsers)




            classes = Class.objects.values('id',
                                            'className',
                                            'creationDate',
                                            'updationDate',
                                            'branchId')
            allTeachersWithClasses = []
            for user in users:
                branches = Branch.objects.filter(id=int(user['branchId']),isActive=True).values('id',
                                                'branchName',
                                                'branchAddress',
                                                'creationDate',
                                                'updationDate',
                                                'schoolId')
                if branches.count() > 0:
                    branch = list(branches)[0]
                    schools = School.objects.filter(id=int(branch['schoolId'])).values('id',
                                                    'schoolName',
                                                    'schoolAddress',
                                                    'creationDate',
                                                    'updationDate')
                    if schools.count() > 0:
                        branch['school'] = list(schools)[0]
                    user['branch'] = branch
                allTeacherClasses = []
                teacherClasses = TeacherClass.objects.filter(teacherId=int(user['id'])).values('id',
                                                                'classId',
                                                                'teacherId',
                                                                'isClassTeacher')
                for teacherClasse in teacherClasses:
                    classes = Class.objects.filter(id=int(teacherClasse['classId']),isActive=True).values('id',
                                                    'className',
                                                    'creationDate',
                                                    'updationDate',
                                                    'branchId')
                    for classe in classes:
                        classe['isClassTeacher'] = teacherClasse['isClassTeacher']
                    if classes.count() > 0:
                        classe = list(classes)[0]
                        branches = Branch.objects.filter(id=int(classe['branchId']),isActive=True).values('id',
                                                        'branchName',
                                                        'branchAddress',
                                                        'creationDate',
                                                        'updationDate',
                                                        'schoolId')
                        if branches.count() > 0:
                            branch = list(branches)[0]
                            schools = School.objects.filter(id=int(branch['schoolId'])).values('id',
                                                            'schoolName',
                                                            'schoolAddress',
                                                            'creationDate',
                                                            'updationDate')
                            if schools.count() > 0:
                                branch['school'] = list(schools)[0]
                            classe['branch'] = branch
                        allTeacherClasses.append(list(classes)[0])
                user['classes'] = allTeacherClasses
                allTeachersWithClasses.append(user)

            if 'filters' in body:
                filters = body['filters']
                for filterObj in filters:
                    if filterObj['filter_name'] == "school":
                        schoolId = int(filterObj['filter_value'])
                        for teacher in list(allTeachersWithClasses):
                            if teacher['branch']['school']['id'] != schoolId:
                                allTeachersWithClasses.remove(teacher)
                    if filterObj['filter_name'] == "branch":
                        branchId = int(filterObj['filter_value'])
                        for teacher in list(allTeachersWithClasses):
                            if teacher['branchId'] != branchId:
                                allTeachersWithClasses.remove(teacher)


            allTeachersWithClasses = sorted(allTeachersWithClasses, key=lambda x: x['updationDate'], reverse=True)
            left = len(allTeachersWithClasses) - (page*itemsPerPage)
            next_page = True
            if left > 0:
                next_page = True
            else:
                next_page = False
            if page == -1:
                return Response({"Teachers": allTeachersWithClasses, "Total" : len(allTeachersWithClasses), "NextPage" : next_page})
            else:
                return Response({"Teachers": allTeachersWithClasses[(page*itemsPerPage)-itemsPerPage:page*itemsPerPage], "Total" : len(allTeachersWithClasses), "NextPage" : next_page})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def GetAllTeachersOfSchool(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            schoolId = body['school_id']
            page = int(body['page'])
            itemsPerPage = int(body['per_page'])
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                

            users = User.objects.filter(type="Teacher",isActive=True).values('id',
                                                            'userName',
                                                            'email',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'isNotification',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'type',
                                                            'studentIdentification',
                                                            'creationDate',
                                                            'updationDate',
                                                            'lastLoginDate',
                                                            'branchId',
                                                            'fullName')
            classes = Class.objects.values('id',
                                            'className',
                                            'creationDate',
                                            'updationDate',
                                            'branchId')
            allTeachersWithClasses = []
            for user in users:
                branches = Branch.objects.filter(id=int(user['branchId']),isActive=True).values('id',
                                                'branchName',
                                                'branchAddress',
                                                'creationDate',
                                                'updationDate',
                                                'schoolId')
                if branches.count() > 0:
                    branch = list(branches)[0]
                    schools = School.objects.filter(id=int(branch['schoolId'])).values('id',
                                                    'schoolName',
                                                    'schoolAddress',
                                                    'creationDate',
                                                    'updationDate')
                    if schools.count() > 0:
                        branch['school'] = list(schools)[0]
                    user['branch'] = branch
                allTeacherClasses = []
                teacherClasses = TeacherClass.objects.filter(teacherId=int(user['id'])).values('id',
                                                                'classId',
                                                                'teacherId',
                                                                'isClassTeacher')
                for teacherClasse in teacherClasses:
                    classes = Class.objects.filter(id=int(teacherClasse['classId']),isActive=True).values('id',
                                                    'className',
                                                    'creationDate',
                                                    'updationDate',
                                                    'branchId')
                    for classe in classes:
                        classe['isClassTeacher'] = teacherClasse['isClassTeacher']
                    if classes.count() > 0:
                        classe = list(classes)[0]
                        branches = Branch.objects.filter(id=int(classe['branchId']),isActive=True).values('id',
                                                        'branchName',
                                                        'branchAddress',
                                                        'creationDate',
                                                        'updationDate',
                                                        'schoolId')
                        if branches.count() > 0:
                            branch = list(branches)[0]
                            schools = School.objects.filter(id=int(branch['schoolId']),isActive=True).values('id',
                                                            'schoolName',
                                                            'schoolAddress',
                                                            'creationDate',
                                                            'updationDate')
                            if schools.count() > 0:
                                school = list(schools)[0]
                                if int(school['id']) != int(schoolId):
                                    break
                                    break
                                branch['school'] = list(schools)[0]
                            classe['branch'] = branch
                        allTeacherClasses.append(list(classes)[0])
                user['classes'] = allTeacherClasses
                allTeachersWithClasses.append(user)
            allTeachersWithClasses = sorted(allTeachersWithClasses, key=lambda x: x['updationDate'], reverse=True)
            left = len(allTeachersWithClasses) - (page*itemsPerPage)
            next_page = True
            if left > 0:
                next_page = True
            else:
                next_page = False
            if page == -1:
                return Response({"Teachers": allTeachersWithClasses, "Total" : len(allTeachersWithClasses), "NextPage" : next_page})
            else:
                return Response({"Teachers": allTeachersWithClasses[(page*itemsPerPage)-itemsPerPage:page*itemsPerPage], "Total" : len(allTeachersWithClasses), "NextPage" : next_page})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def GetAllTeachersOfBranch(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            branchId = body['branch_id']
            page = int(body['page'])
            itemsPerPage = int(body['per_page'])
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                

            users = User.objects.filter(type="Teacher",isActive=True).values('id',
                                                            'userName',
                                                            'email',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'isNotification',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'type',
                                                            'studentIdentification',
                                                            'creationDate',
                                                            'updationDate',
                                                            'lastLoginDate',
                                                            'branchId',
                                                            'fullName')
            classes = Class.objects.values('id',
                                            'className',
                                            'creationDate',
                                            'updationDate',
                                            'branchId')
            allTeachersWithClasses = []
            for user in users:
                branches = Branch.objects.filter(id=int(user['branchId']),isActive=True).values('id',
                                                'branchName',
                                                'branchAddress',
                                                'creationDate',
                                                'updationDate',
                                                'schoolId')
                if branches.count() > 0:
                    branch = list(branches)[0]
                    schools = School.objects.filter(id=int(branch['schoolId'])).values('id',
                                                    'schoolName',
                                                    'schoolAddress',
                                                    'creationDate',
                                                    'updationDate')
                    if schools.count() > 0:
                        branch['school'] = list(schools)[0]
                    user['branch'] = branch
                allTeacherClasses = []
                teacherClasses = TeacherClass.objects.filter(teacherId=int(user['id'])).values('id',
                                                                'classId',
                                                                'teacherId',
                                                                'isClassTeacher')
                for teacherClasse in teacherClasses:
                    classes = Class.objects.filter(id=int(teacherClasse['classId']),isActive=True).values('id',
                                                    'className',
                                                    'creationDate',
                                                    'updationDate',
                                                    'branchId')
                    for classe in classes:
                        classe['isClassTeacher'] = teacherClasse['isClassTeacher']
                    if classes.count() > 0:
                        classe = list(classes)[0]
                        branches = Branch.objects.filter(id=int(classe['branchId']),isActive=True).values('id',
                                                        'branchName',
                                                        'branchAddress',
                                                        'creationDate',
                                                        'updationDate',
                                                        'schoolId')
                        if branches.count() > 0:
                            branch = list(branches)[0]
                            if int(branch['id']) != int(branchId):
                                break
                                break
                            schools = School.objects.filter(id=int(branch['schoolId']),isActive=True).values('id',
                                                            'schoolName',
                                                            'schoolAddress',
                                                            'creationDate',
                                                            'updationDate')
                            if schools.count() > 0:
                                branch['school'] = list(schools)[0]
                            classe['branch'] = branch
                        allTeacherClasses.append(list(classes)[0])
                user['classes'] = allTeacherClasses
                allTeachersWithClasses.append(user)
            allTeachersWithClasses = sorted(allTeachersWithClasses, key=lambda x: x['updationDate'], reverse=True)
            left = len(allTeachersWithClasses) - (page*itemsPerPage)
            next_page = True
            if left > 0:
                next_page = True
            else:
                next_page = False
            if page == -1:
                return Response({"Teachers": allTeachersWithClasses, "Total" : len(allTeachersWithClasses), "NextPage" : next_page})
            else:
                return Response({"Teachers": allTeachersWithClasses[(page*itemsPerPage)-itemsPerPage:page*itemsPerPage], "Total" : len(allTeachersWithClasses), "NextPage" : next_page})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def GetAllStudents(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            page = int(body['page'])
            itemsPerPage = int(body['per_page'])
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] == "Student" or user['type'] == "Parent":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to view All Students."})
            users = User.objects.filter(type="Student",isActive=True).values('id',
                                                            'userName',
                                                            'email',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'isNotification',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'type',
                                                            'studentIdentification',
                                                            'creationDate',
                                                            'updationDate',
                                                            'lastLoginDate')

            users = sorted(users, key=lambda x: x['updationDate'], reverse=True)
            left = len(users) - (page*itemsPerPage)
            next_page = True
            if left > 0:
                next_page = True
            else:
                next_page = False
            if page == -1:
                return Response({"Students": users, "Total" : len(users), "NextPage" : next_page})
            else:
                return Response({"Students": users[(page*itemsPerPage)-itemsPerPage:page*itemsPerPage], "Total" : len(users), "NextPage" : next_page})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def GetAllStudentsOfBranch(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            branchId = body['branch_id']
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] == "Student":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to view All Branch Students."})
            users = User.objects.filter(type="Student",branchId=int(branchId),isActive=True).values('id',
                                                            'userName',
                                                            'email',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'isNotification',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'type',
                                                            'studentIdentification',
                                                            'creationDate',
                                                            'updationDate',
                                                            'lastLoginDate',
                                                            'branchId')

            users = sorted(users, key=lambda x: x['updationDate'], reverse=True)
            return Response({"Students": users, "Total" : len(users)})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})








@api_view(['POST', ])
def getParentsOfStudent(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            studentId = body['student_id']
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] == "Student" :
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to see student's parent."})
            studentUsers = User.objects.filter(id=int(studentId),isActive=True).values('type')
            if studentUsers.count() == 0:
                return Response({"Error" :"No User Found With the specified id."})
            else:
                studentUser = list(studentUsers)[0]
                if studentUser['type'] != "Student":
                    return Response({"Error" :"Specified User is not Student."})
            studentParents = StudentParent.objects.filter(studentId=int(studentId)).values('studentId','parentId')
            allParents = []
            for studentParent in studentParents:
                users = User.objects.filter(id=int(studentParent['parentId']),isActive=True).values('id',
                                                                'userName',
                                                                'email',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'isNotification',
                                                                'profilePictureURL',
                                                                'deviceToken',
                                                                'type',
                                                                'studentIdentification',
                                                                'creationDate',
                                                                'updationDate',
                                                                'lastLoginDate',
                                                                'fullName')
                allParents.extend(users)
            return Response({"Parents" :list(allParents)})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getChildrenOfParent(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            parentId = body['parent_id']
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] == "Student" :
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to see student's parent."})
            parentUsers = User.objects.filter(id=int(parentId),isActive=True).values('type')
            if parentUsers.count() == 0:
                return Response({"Error" :"No User Found With the specified id."})
            else:
                parentUser = list(parentUsers)[0]
                if parentUser['type'] != "Parent":
                    return Response({"Error" :"Specified User is not Parent."})
            studentParents = StudentParent.objects.filter(parentId=int(parentId)).values('studentId','parentId')
            allChildren = []
            for studentParent in studentParents:
                users = User.objects.filter(id=int(studentParent['studentId']),isActive=True).values('id',
                                                                'userName',
                                                                'email',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'isNotification',
                                                                'profilePictureURL',
                                                                'deviceToken',
                                                                'type',
                                                                'studentIdentification',
                                                                'creationDate',
                                                                'updationDate',
                                                                'lastLoginDate',
                                                                'fullName')
                allChildren.extend(users)
            return Response({"Children" :list(allChildren)})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})




@api_view(['POST', ])
def getAllParents(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            page = int(body['page'])
            itemsPerPage = int(body['per_page'])
            users = User.objects.filter(authToken=str(auth)).values('type','branchId')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] == "Student" :
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to see student's parent."})
            allParentUsers = User.objects.filter(type='Parent',isActive=True).values('id',
                                                                    'userName',
                                                                    'email',
                                                                    'age',
                                                                    'phone',
                                                                    'gender',
                                                                    'isNotification',
                                                                    'profilePictureURL',
                                                                    'deviceToken',
                                                                    'type',
                                                                    'creationDate',
                                                                    'updationDate',
                                                                    'lastLoginDate',
                                                                    'branchId',
                                                                    'fullName')



            if user['type'] == "S-Admin":
                allParentUsers = []
                schools = School.objects.filter(id=int(user['branchId'])).values('id',
                                                'schoolName',
                                                'schoolAddress',
                                                'creationDate',
                                                'updationDate')
                for school in schools:
                    branches = Branch.objects.filter(schoolId=int(school['id'])).values('id',
                                                    'branchName',
                                                    'branchAddress',
                                                    'creationDate',
                                                    'updationDate',
                                                    'schoolId')
                    for branch in branches:
                        branchUsers = User.objects.filter(type="Parent",branchId=int(branch['id']),isActive=True).values('id',
                                                                        'userName',
                                                                        'email',
                                                                        'age',
                                                                        'phone',
                                                                        'gender',
                                                                        'isNotification',
                                                                        'profilePictureURL',
                                                                        'deviceToken',
                                                                        'type',
                                                                        'studentIdentification',
                                                                        'creationDate',
                                                                        'updationDate',
                                                                        'lastLoginDate',
                                                                        'branchId',
                                                                        'fullName')
                        allParentUsers.extend(branchUsers)


            if user['type'] == "B-Admin":
                allParentUsers = []
                branches = Branch.objects.filter(id=int(user['branchId'])).values('id',
                                                'branchName',
                                                'branchAddress',
                                                'creationDate',
                                                'updationDate',
                                                'schoolId')
                for branch in branches:
                    branchUsers = User.objects.filter(type="Parent",branchId=int(branch['id']),isActive=True).values('id',
                                                                    'userName',
                                                                    'email',
                                                                    'age',
                                                                    'phone',
                                                                    'gender',
                                                                    'isNotification',
                                                                    'profilePictureURL',
                                                                    'deviceToken',
                                                                    'type',
                                                                    'studentIdentification',
                                                                    'creationDate',
                                                                    'updationDate',
                                                                    'lastLoginDate',
                                                                    'branchId',
                                                                    'fullName')
                    allParentUsers.extend(branchUsers)



            allParentUsers = sorted(allParentUsers, key=lambda x: x['updationDate'], reverse=True)
            for parentUser in allParentUsers:
                branches = Branch.objects.filter(id=int(parentUser['branchId']),isActive=True).values('id',
                                                'branchName',
                                                'branchAddress',
                                                'creationDate',
                                                'updationDate',
                                                'schoolId')
                if branches.count() > 0:
                    branch = list(branches)[0]
                    schools = School.objects.filter(id=int(branch['schoolId'])).values('id',
                                                    'schoolName',
                                                    'schoolAddress',
                                                    'creationDate',
                                                    'updationDate')
                    if schools.count() > 0:
                        branch['school'] = list(schools)[0]
                    parentUser['branch'] = branch

                studentParents = StudentParent.objects.filter(parentId=int(parentUser['id'])).values('studentId','parentId')
                allChildren = []
                for studentParent in studentParents:
                    users = User.objects.filter(id=int(studentParent['studentId']),isActive=True).values('id',
                                                                    'userName',
                                                                    'email',
                                                                    'age',
                                                                    'phone',
                                                                    'gender',
                                                                    'isNotification',
                                                                    'profilePictureURL',
                                                                    'deviceToken',
                                                                    'type',
                                                                    'studentIdentification',
                                                                    'creationDate',
                                                                    'updationDate',
                                                                    'lastLoginDate')
                    allChildren.extend(users)
                for child in allChildren:
                    childStudentClasses = StudentClass.objects.filter(studentId=int(child['id'])).values('classId')
                    for childStudentClasse in childStudentClasses:
                        classes = Class.objects.filter(id=int(childStudentClasse['classId']),isActive=True).values('className')
                        for classe in classes:
                            child['gender'] = classe['className']
                    
                    
                parentUser['childs'] = allChildren
            if 'filters' in body:
                filters = body['filters']
                for filterObj in filters:
                    if filterObj['filter_name'] == "school":
                        schoolId = int(filterObj['filter_value'])
                        for parent in list(allParentUsers):
                            if parent['branch']['school']['id'] != schoolId:
                                allParentUsers.remove(parent)
                    if filterObj['filter_name'] == "branch":
                        branchId = int(filterObj['filter_value'])
                        for parent in list(allParentUsers):
                            if parent['branchId'] != branchId:
                                allParentUsers.remove(parent)

            allParentUsers = sorted(allParentUsers, key=lambda x: x['updationDate'], reverse=True)
            left = len(allParentUsers) - (page * itemsPerPage)
            next_page = True
            if left > 0:
                next_page = True
            else:
                next_page = False
            if page == -1:
                return Response({"Parents": allParentUsers, "Total": len(allParentUsers), "NextPage": next_page})
            else:
                return Response({"Parents": allParentUsers[(page * itemsPerPage) - itemsPerPage:page * itemsPerPage],
                                 "Total": len(allParentUsers), "NextPage": next_page})

        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def AddParentWithStudents(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            userName = str(body['parent_user_name']).lower()
            email = str(body['parent_email']).lower()
            password = body['parent_password']
            age = body['parent_age']
            phone = body['parent_phone']
            gender = body['parent_gender']
            branchId = body['branch_id']
            profilePictureURL = ""
            fullName = body['parent_full_name']
            type = "Parent"
            childs = body['parent_childs']
            users = User.objects.filter(authToken=str(auth)).values('type','id')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if type == "Parent":
                    if user['type'] == "Teacher" or user['type'] == "Student" or user['type'] == "Parent":
                        return Response({"Error" :"User of type "+user['type']+" do not have ability to add new Parent."})
            alreadyUsers = User.objects.filter(userName=str(userName))
            if alreadyUsers.count() > 0:
                return Response({"Error" :"There is already a User present with this Username"})
            for childDict in childs:
                mediaData = childDict['profile_picture_data']
                extension = ".jpg"
                contentType = "image/jpeg"
                connection = Utility.make_AWS_Connection()
                bucket = connection.get_bucket(Constants.AWS_BUCKET_NAME())
                cloudfile = Key(bucket)
                millis = int(round(time.time() * 1000))
                path = "KidsApp/" + str(user['id']) + str(millis) + ".jpg"
                cloudfile.key = path
                cloudfile.set_contents_from_string(base64.b64decode(mediaData))
                cloudfile.set_metadata('Content-Type', contentType)
                cloudfile.set_acl('public-read')
                childDict['profile_picture_link'] = Constants.AWS_BASE_URL() + path
            new = User(userName=str(userName),
                    email=str(email),
                    password=str(password),
                    age=str(age),
                    phone=str(phone),
                    gender=str(gender),
                    isNotification=True,
                    profilePictureURL=str(profilePictureURL),
                    deviceToken="",
                    type=str(type),
                    studentIdentification="",
                    isPasswordPinChangeNeeded=True,
                    branchId=int(branchId),
                    fullName=fullName)
            new.save()
            parentId = new.id
            for childDict in childs:
                newChild = User(userName=str(childDict['name']),
                        email="",
                        password="",
                        age=str(childDict['age']),
                        phone="",
                        gender=str(childDict['gender']),
                        isNotification=False,
                        profilePictureURL=childDict['profile_picture_link'],
                        deviceToken="",
                        type="Student",
                        studentIdentification="",
                        isPasswordPinChangeNeeded=False,
                        branchId=int(branchId))
                newChild.save()
                studentId = newChild.id
                newStudentParent = StudentParent(studentId=int(studentId),parentId=int(parentId))
                newStudentParent.save()

                if 'class_id' in childDict:
                    newStudentClass = StudentClass(classId=int(childDict['class_id']),studentId=int(studentId))
                    newStudentClass.save()

            parents = User.objects.filter(id=int(parentId),isActive=True).values('id',
                                                            'userName',
                                                            'email',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'isNotification',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'type',
                                                            'studentIdentification',
                                                            'creationDate',
                                                            'updationDate',
                                                            'lastLoginDate',
                                                            'branchId',
                                                            'fullName')
            parent = list(parents)[0]
            allParentStudents = []

            studentParents = StudentParent.objects.filter(parentId=int(parent['id'])).values('id',
                                                            'studentId',
                                                            'parentId')
            for studentParent in studentParents:
                users = User.objects.filter(id=int(studentParent['studentId']),isActive=True).values('id',
                                                                'userName',
                                                                'age',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'type',
                                                                'creationDate',
                                                                'updationDate',
                                                                'branchId')
                if users.count() > 0:
                    allParentStudents.append(list(users)[0])
            parent['students'] = allParentStudents
            return Response({"NewParent" :parent})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})





@api_view(['POST', ])
def EditTeacher(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            teacherID = int(body['teacher_id'])
            users = User.objects.filter(authToken=str(auth)).values('type','id')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]

            alreadyUsers = User.objects.filter(id=int(teacherID),isActive=True)
            if alreadyUsers.count() == 0:
                return Response({"Error" :"No Teacher Found with Specified ID"})
            for i in body["TeacherData"]:
                if i == "user_name":
                    for j in alreadyUsers:
                        j.userName = str(body["TeacherData"]['user_name']).lower()
                        j.save()
                if i == "email":
                    for j in alreadyUsers:
                        j.email = str(body["TeacherData"]['email']).lower()
                        j.save()

                if i == "password":
                    for j in alreadyUsers:
                        j.password = body["TeacherData"]['password']
                        j.save()

                if i == "age":
                    for j in alreadyUsers:
                        j.age = body["TeacherData"]['age']
                        j.save()

                if i == "phone":
                    for j in alreadyUsers:
                        j.phone = body["TeacherData"]['phone']
                        j.save()

                if i == "gender":
                    for j in alreadyUsers:
                        j.gender = body["TeacherData"]['gender']
                        j.save()

                if i == "is_notification":
                    for j in alreadyUsers:
                        j.isNotification = body["TeacherData"]['is_notification']
                        j.save()

                if i == "branch_id":
                    for j in alreadyUsers:
                        j.branchId = body["TeacherData"]['branch_id']
                        j.save()

                if i == "full_name":
                    for j in alreadyUsers:
                        j.fullName = body["TeacherData"]['full_name']
                        j.save()

                if i == "class_ids":
                    for j in alreadyUsers:
                        teacherClasses = TeacherClass.objects.filter(teacherId=int(j.id))
                        for teacherClasse in teacherClasses:
                            teacherClasse.delete()
                        classesArray = body["TeacherData"]['class_ids']
                        for classe in classesArray:
                            classId = classe['class_id']
                            isClassTeacher = False
                            if 'is_class_teacher' in classe:
                                isClassTeacher = classe['is_class_teacher']
                            newTeacherClass = TeacherClass(classId=int(classId),teacherId=int(j.id),isClassTeacher=isClassTeacher)
                            newTeacherClass.save()
                if i == "profile_picture_data":
                    for j in alreadyUsers:
                        profilePictureDATA = body["TeacherData"]['profile_picture_data']
                        mediaData = profilePictureDATA
                        extension = ".jpg"
                        contentType = "image/jpeg"
                        connection = Utility.make_AWS_Connection()
                        bucket = connection.get_bucket(Constants.AWS_BUCKET_NAME())
                        cloudfile = Key(bucket)
                        millis = int(round(time.time() * 1000))
                        path = "KidsApp/" + str(user['id']) + str(millis) + ".jpg"
                        cloudfile.key = path
                        cloudfile.set_contents_from_string(base64.b64decode(mediaData))
                        cloudfile.set_metadata('Content-Type', contentType)
                        cloudfile.set_acl('public-read')
                        profilePictureURL = Constants.AWS_BASE_URL() + path
                        j.profilePictureURL = profilePictureURL
                        j.save()

            teacherUsers = User.objects.filter(id=int(teacherID),isActive=True).values('id',
                                                            'userName',
                                                            'email',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'isNotification',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'type',
                                                            'studentIdentification',
                                                            'creationDate',
                                                            'updationDate',
                                                            'lastLoginDate',
                                                            'isPasswordPinChangeNeeded',
                                                            'branchId',
                                                            'fullName')
            teacher = list(teacherUsers)[0]
            allTeacherClasses = []
            teacherClasses = TeacherClass.objects.filter(teacherId=int(teacher['id'])).values('id',
                                                            'classId',
                                                            'teacherId',
                                                            'isClassTeacher')
            for teacherClasse in teacherClasses:
                classes = Class.objects.filter(id=int(teacherClasse['classId']),isActive=True).values('id',
                                                                'className',
                                                                'creationDate',
                                                                'updationDate')
                for classe in classes:
                    classe['isClassTeacher'] = teacherClasse['isClassTeacher']
                allTeacherClasses.extend(classes)
            teacher['classes'] = allTeacherClasses
            return Response({"EditedTeacher" :teacher})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def RemoveTeacher(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            teacherID = int(body['teacher_id'])
            users = User.objects.filter(authToken=str(auth)).values('type','id')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                

            alreadyUsers = User.objects.filter(id=int(teacherID),isActive=True)
            if alreadyUsers.count() == 0:
                return Response({"Error" :"No Teacher Found with Specified ID"})
            for j in alreadyUsers:
                j.isActive = False
                j.save()
                teacherClasses = TeacherClass.objects.filter(teacherId=int(j.id))
                for teacherClasse in teacherClasses:
                    teacherClasse.delete()
                createdPosts = Post.objects.filter(creatorId=int(j.id),isActive=True)
                for createdPost in createdPosts:
                    createdPost.isActive = False
                    createdPost.save()
                createdComments = Comment.objects.filter(userId=int(j.id),isActive=True)
                for createdComment in createdComments:
                    createdComment.isActive = False
                    createdComment.save()
                createdLikes = Like.objects.filter(userId=int(j.id))
                for createdLike in createdLikes:
                    createdLike.delete()
            return Response({"EditedTeacher" :"Teacher has been removed."})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def GetDetailsOfTeacher(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            teacherID = int(body['teacher_id'])
            users = User.objects.filter(authToken=str(auth)).values('type','id')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                
            teacherUsers = User.objects.filter(id=int(teacherID),isActive=True).values('id',
                                                            'userName',
                                                            'email',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'isNotification',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'type',
                                                            'studentIdentification',
                                                            'creationDate',
                                                            'updationDate',
                                                            'lastLoginDate',
                                                            'isPasswordPinChangeNeeded',
                                                            'branchId',
                                                            'fullName')
            teacher = list(teacherUsers)[0]
            allTeacherClasses = []
            teacherClasses = TeacherClass.objects.filter(teacherId=int(teacher['id'])).values('id',
                                                            'classId',
                                                            'teacherId',
                                                            'isClassTeacher')
            for teacherClasse in teacherClasses:
                classes = Class.objects.filter(id=int(teacherClasse['classId']),isActive=True).values('id',
                                                                'className',
                                                                'creationDate',
                                                                'updationDate',
                                                                'branchId')
                for classe in classes:
                    classe['isClassTeacher'] = teacherClasse['isClassTeacher']
                    branches = Branch.objects.filter(id=int(classe['branchId']),isActive=True).values('id',
                                                    'branchName',
                                                    'branchAddress',
                                                    'creationDate',
                                                    'updationDate',
                                                    'schoolId')
                    if branches.count() > 0:
                        branch = list(branches)[0]
                        schools = School.objects.filter(id=int(branch['schoolId'])).values('id',
                                                        'schoolName',
                                                        'schoolAddress',
                                                        'creationDate',
                                                        'updationDate')
                        if schools.count() > 0:
                            branch['school'] = list(schools)[0]
                        classe['branch'] = branch
                allTeacherClasses.extend(classes)
            teacher['classes'] = allTeacherClasses
            return Response({"Teacher" :teacher})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def EditParent(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            parentId = body['parent_id']
            users = User.objects.filter(authToken=str(auth)).values('type','id','branchId')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]

            alreadyUsers = User.objects.filter(id=int(parentId),type="Parent",isActive=True)
            if alreadyUsers.count() == 0:
                return Response({"Error" :"No Parent found with specified id."})
            for i in body["ParentData"]:
                if i == "user_name":
                    for j in alreadyUsers:
                        j.userName = str(body["ParentData"]['user_name']).lower()
                        j.save()
                if i == "email":
                    for j in alreadyUsers:
                        j.email = str(body["ParentData"]['email']).lower()
                        j.save()

                if i == "password":
                    for j in alreadyUsers:
                        j.password = body["ParentData"]['password']
                        j.save()

                if i == "age":
                    for j in alreadyUsers:
                        j.age = body["ParentData"]['age']
                        j.save()

                if i == "phone":
                    for j in alreadyUsers:
                        j.phone = body["ParentData"]['phone']
                        j.save()

                if i == "gender":
                    for j in alreadyUsers:
                        j.gender = body["ParentData"]['gender']
                        j.save()

                if i == "is_notification":
                    for j in alreadyUsers:
                        j.isNotification = body["ParentData"]['is_notification']
                        j.save()

                if i == "branch_id":
                    for j in alreadyUsers:
                        j.branchId = body["ParentData"]['branch_id']
                        j.save()

                if i == "full_name":
                    for j in alreadyUsers:
                        j.fullName = body["ParentData"]['full_name']
                        j.save()


                if i == "childs":
                    for j in alreadyUsers:
                        studentParents = StudentParent.objects.filter(parentId=int(j.id))
                        for studentParent in studentParents:
                            students = User.objects.filter(id=int(studentParent.studentId),isActive=True)
                            for student in students:
                                student.isActive = False
                                student.save()
                            studentParent.delete()
                        childArray = body["ParentData"]['childs']
                        for child in childArray:
                            childId = 0
                            if not('child_id' in child):
                                mediaData = child['profile_picture_data']
                                extension = ".jpg"
                                contentType = "image/jpeg"
                                AWS_ACCESS_KEY = 'AKIAJBOEQASD4L7YUKFQ'
                                AWS_SECRET_KEY = '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'
                                AWS_BUCKET_NAME = 'uchoose'
                                connection = boto.s3.connect_to_region('us-east-1',
                                                                        aws_access_key_id=AWS_ACCESS_KEY,
                                                                        aws_secret_access_key=AWS_SECRET_KEY,
                                                                        #is_secure=True,               # uncomment if you are not using ssl
                                                                        calling_format = boto.s3.connection.OrdinaryCallingFormat(),
                                                                        )
                                bucket = connection.get_bucket(AWS_BUCKET_NAME)
                                cloudfile = Key(bucket)
                                millis = int(round(time.time() * 1000))
                                path = "KidsApp/" + str(user['id']) + str(millis) + ".jpg"
                                cloudfile.key = path
                                cloudfile.set_contents_from_string(base64.b64decode(mediaData))
                                cloudfile.set_metadata('Content-Type', contentType)
                                cloudfile.set_acl('public-read')
                                child['profile_picture_link'] = "https://s3.amazonaws.com/uchoose/" + path




                                newChild = User(userName=str(child['name']),
                                        email="",
                                        password="",
                                        age=str(child['age']),
                                        phone="",
                                        gender=str(child['gender']),
                                        isNotification=False,
                                        profilePictureURL=child['profile_picture_link'],
                                        deviceToken="",
                                        type="Student",
                                        studentIdentification="",
                                        isPasswordPinChangeNeeded=False,
                                        branchId=int(user['branchId']))
                                newChild.save()
                                childId = newChild.id
                            else:
                                childId = child['child_id']
                                students = User.objects.filter(id=int(childId))
                                if ('name' in child):
                                    for student in students:
                                        student.userName = str(child['name'])
                                        student.save()
                                if ('age' in child):
                                    for student in students:
                                        student.age = str(child['age'])
                                        student.save()
                                if ('gender' in child):
                                    for student in students:
                                        student.gender = str(child['gender'])
                                        student.save()
                                if ('class_id' in child):
                                    for student in students:
                                        previousStudentClasses = StudentClass.objects.filter(studentId=int(student.id))
                                        previousStudentClasses.delete()
                                        newStudentClass = StudentClass(classId=int(child['class_id']),studentId=int(student.id))
                                        newStudentClass.save()

                                if ('profile_picture_data' in child):
                                    mediaData = child['profile_picture_data']
                                    extension = ".jpg"
                                    contentType = "image/jpeg"
                                    AWS_ACCESS_KEY = 'AKIAJBOEQASD4L7YUKFQ'
                                    AWS_SECRET_KEY = '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'
                                    AWS_BUCKET_NAME = 'uchoose'
                                    connection = boto.s3.connect_to_region('us-east-1',
                                                                            aws_access_key_id=AWS_ACCESS_KEY,
                                                                            aws_secret_access_key=AWS_SECRET_KEY,
                                                                            #is_secure=True,               # uncomment if you are not using ssl
                                                                            calling_format = boto.s3.connection.OrdinaryCallingFormat(),
                                                                            )
                                    bucket = connection.get_bucket(AWS_BUCKET_NAME)
                                    cloudfile = Key(bucket)
                                    millis = int(round(time.time() * 1000))
                                    path = "KidsApp/" + str(user['id']) + str(millis) + ".jpg"
                                    cloudfile.key = path
                                    cloudfile.set_contents_from_string(base64.b64decode(mediaData))
                                    cloudfile.set_metadata('Content-Type', contentType)
                                    cloudfile.set_acl('public-read')
                                    child['profile_picture_link'] = "https://s3.amazonaws.com/uchoose/" + path
                                    for student in students:
                                        student.profilePictureURL = str(child['profile_picture_link'])
                                        student.save()



                            newStudentParent = StudentParent(studentId=int(childId),parentId=int(j.id))
                            newStudentParent.save()
                            students = User.objects.filter(id=int(newStudentParent.studentId))
                            for student in students:
                                student.isActive = True
                                student.save()
            parents = User.objects.filter(id=int(parentId),isActive=True).values('id',
                                                            'userName',
                                                            'email',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'isNotification',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'type',
                                                            'studentIdentification',
                                                            'creationDate',
                                                            'updationDate',
                                                            'lastLoginDate',
                                                            'branchId',
                                                            'fullName')
            parent = list(parents)[0]
            allParentStudents = []
            studentParents = StudentParent.objects.filter(parentId=int(parent['id'])).values('id',
                                                            'studentId',
                                                            'parentId')
            for studentParent in studentParents:
                users = User.objects.filter(id=int(studentParent['studentId']),isActive=True).values('id',
                                                                'userName',
                                                                'age',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'type',
                                                                'creationDate',
                                                                'updationDate',
                                                                'branchId')
                if users.count() > 0:
                    allParentStudents.append(list(users)[0])
            parent['students'] = allParentStudents
            return Response({"EditedParent" :parent})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def RemoveParent(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            parentId = body['parent_id']
            users = User.objects.filter(authToken=str(auth)).values('type','id')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
            
            alreadyUsers = User.objects.filter(id=int(parentId),type="Parent",isActive=True)
            if alreadyUsers.count() == 0:
                return Response({"Error" :"No Parent found with specified id."})
            for j in alreadyUsers:
                j.isActive = False
                j.save()
                createdPosts = Post.objects.filter(creatorId=int(j.id),isActive=True)
                for createdPost in createdPosts:
                    createdPost.isActive = False
                    createdPost.save()
                createdComments = Comment.objects.filter(userId=int(j.id),isActive=True)
                for createdComment in createdComments:
                    createdComment.isActive = False
                    createdComment.save()
                createdLikes = Like.objects.filter(userId=int(j.id))
                for createdLike in createdLikes:
                    createdLike.delete()
                studentParents = StudentParent.objects.filter(parentId=int(j.id)).values('id',
                                                                'studentId',
                                                                'parentId')
                for studentParent in studentParents:
                    users = User.objects.filter(id=int(studentParent['studentId']),isActive=True)
                    for j in users:
                        j.isActive = False
                        j.save()
            return Response({"Status" :"Parent has been removed."})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def GetDetailsOfParent(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            parentId = body['parent_id']
            users = User.objects.filter(authToken=str(auth)).values('type','id')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
            alreadyUsers = User.objects.filter(id=int(parentId),type="Parent",isActive=True)
            if alreadyUsers.count() == 0:
                return Response({"Error" :"No Parent found with specified id."})
            parents = User.objects.filter(id=int(parentId),isActive=True).values('id',
                                                            'userName',
                                                            'email',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'isNotification',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'type',
                                                            'studentIdentification',
                                                            'creationDate',
                                                            'updationDate',
                                                            'lastLoginDate',
                                                            'branchId',
                                                            'fullName')
            parent = list(parents)[0]

            branches = Branch.objects.filter(id=int(parent['branchId']),isActive=True).values('id',
                                            'branchName',
                                            'branchAddress',
                                            'creationDate',
                                            'updationDate',
                                            'schoolId')
            if branches.count() > 0:
                branch = list(branches)[0]
                schools = School.objects.filter(id=int(branch['schoolId'])).values('id',
                                                'schoolName',
                                                'schoolAddress',
                                                'creationDate',
                                                'updationDate')
                if schools.count() > 0:
                    branch['school'] = list(schools)[0]
                parent['branch'] = branch


            allParentStudents = []
            studentParents = StudentParent.objects.filter(parentId=int(parent['id'])).values('id',
                                                            'studentId',
                                                            'parentId')
            for studentParent in studentParents:
                users = User.objects.filter(id=int(studentParent['studentId']),isActive=True).values('id',
                                                                'userName',
                                                                'age',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'type',
                                                                'creationDate',
                                                                'updationDate',
                                                                'branchId')
                if users.count() > 0:
                    allParentStudents.append(list(users)[0])
            parent['students'] = allParentStudents
            return Response({"Parent" :parent})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def GetAllAdmins(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            page = int(body['page'])
            itemsPerPage = int(body['per_page'])
            adminUsers = User.objects.filter(authToken=str(auth)).values('type','branchId')
            if adminUsers.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                adminUser = list(adminUsers)[0]
                if adminUser['type'] == "Teacher" or adminUser['type'] == "Parent":
                    return Response({"Error" :"User of type "+adminUser['type']+" do not have ability to view All Admins."})

            users = []
            if adminUser['type'] == "SuperAdmin":
                users = User.objects.filter(Q(type="SuperAdmin") | Q(type="S-Admin") | Q(type="B-Admin"),isActive=True).values('id',
                                                                'userName',
                                                                'email',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'isNotification',
                                                                'profilePictureURL',
                                                                'deviceToken',
                                                                'type',
                                                                'studentIdentification',
                                                                'creationDate',
                                                                'updationDate',
                                                                'lastLoginDate',
                                                                'branchId',
                                                                'fullName')


            allAdmins = []
            for user in users:
                if user['type'] == "S-Admin":
                    user['type'] = "SchoolAdmin"
                    schools = School.objects.filter(id=int(user['branchId'])).values('id',
                                                    'schoolName',
                                                    'schoolAddress',
                                                    'creationDate',
                                                    'updationDate')
                    if schools.count() > 0:
                        user['school'] = list(schools)[0]

                if user['type'] == "B-Admin":
                    user['type'] = "BranchAdmin"
                    branches = Branch.objects.filter(id=int(user['branchId']),isActive=True).values('id',
                                                    'branchName',
                                                    'branchAddress',
                                                    'creationDate',
                                                    'updationDate',
                                                    'schoolId')
                    if branches.count() > 0:
                        branch = list(branches)[0]
                        schools = School.objects.filter(id=int(branch['schoolId'])).values('id',
                                                        'schoolName',
                                                        'schoolAddress',
                                                        'creationDate',
                                                        'updationDate')
                        if schools.count() > 0:
                            branch['school'] = list(schools)[0]
                        user['branch'] = branch
                allAdmins.append(user)

            if 'filters' in body:
                filters = body['filters']
                for filterObj in filters:
                    if filterObj['filter_name'] == "school":
                        schoolId = int(filterObj['filter_value'])
                        for teacher in allAdmins:
                            if teacher['branch']['school']['id'] != schoolId:
                                allAdmins.remove(teacher)
                    if filterObj['filter_name'] == "branch":
                        branchId = int(filterObj['filter_value'])
                        for teacher in allAdmins:
                            if teacher['branchId'] != branchId:
                                allAdmins.remove(teacher)


            allAdmins = sorted(allAdmins, key=lambda x: x['updationDate'], reverse=True)
            left = len(allAdmins) - (page*itemsPerPage)
            next_page = True
            if left > 0:
                next_page = True
            else:
                next_page = False
            if page == -1:
                return Response({"Admins": allAdmins, "Total" : len(allAdmins), "NextPage" : next_page})
            else:
                return Response({"Admins": allAdmins[(page*itemsPerPage)-itemsPerPage:page*itemsPerPage], "Total" : len(allAdmins), "NextPage" : next_page})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def GetDetailsOfAdmin(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            adminID = int(body['admin_id'])
            adminUsers = User.objects.filter(authToken=str(auth)).values('type','branchId')
            if adminUsers.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                adminUser = list(adminUsers)[0]
                if adminUser['type'] == "Teacher" or adminUser['type'] == "Parent":
                    return Response({"Error" :"User of type "+adminUser['type']+" do not have ability to view All Admins."})

            users = []
            if adminUser['type'] == "SuperAdmin":
                users = User.objects.filter(id=int(adminID),isActive=True).values('id',
                                                                'userName',
                                                                'email',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'isNotification',
                                                                'profilePictureURL',
                                                                'deviceToken',
                                                                'type',
                                                                'studentIdentification',
                                                                'creationDate',
                                                                'updationDate',
                                                                'lastLoginDate',
                                                                'branchId',
                                                                'fullName')


            for user in users:
                if user['type'] == "S-Admin":
                    user['type'] = "SchoolAdmin"
                    schools = School.objects.filter(id=int(user['branchId'])).values('id',
                                                    'schoolName',
                                                    'schoolAddress',
                                                    'creationDate',
                                                    'updationDate')
                    if schools.count() > 0:
                        user['school'] = list(schools)[0]

                if user['type'] == "B-Admin":
                    user['type'] = "BranchAdmin"
                    branches = Branch.objects.filter(id=int(user['branchId']),isActive=True).values('id',
                                                    'branchName',
                                                    'branchAddress',
                                                    'creationDate',
                                                    'updationDate',
                                                    'schoolId')
                    if branches.count() > 0:
                        branch = list(branches)[0]
                        schools = School.objects.filter(id=int(branch['schoolId'])).values('id',
                                                        'schoolName',
                                                        'schoolAddress',
                                                        'creationDate',
                                                        'updationDate')
                        if schools.count() > 0:
                            branch['school'] = list(schools)[0]
                        user['branch'] = branch


            return Response({"Admin": list(users)[0]})

        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})
