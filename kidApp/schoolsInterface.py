import base64
from rest_framework.response import Response
from rest_framework.decorators import api_view
from kidApp.models import User, School, Branch, Class, TeacherClass, StudentParent, Post, Comment, Like, Media, Tag
import uuid
import random
import json
from KidsApp.settings import MEDIA_URL, MEDIA_ROOT


@api_view(['POST', ])
def AddSchool(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            schoolName = body['school_name']
            schoolAddress = body['school_address']
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] != "SuperAdmin":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to add schools."})
            allSchools = School.objects.filter(schoolName=str(schoolName),schoolAddress=str(schoolAddress))
            if allSchools.count() > 0:
                return Response({"Error" :"There is already a school present with this name and address"})
            new = School(schoolName=str(schoolName),schoolAddress=str(schoolAddress))
            new.save()
            schools = School.objects.filter(id=new.id).values('id',
                                                            'schoolName',
                                                            'schoolAddress',
                                                            'creationDate',
                                                            'updationDate')
            school = list(schools)[0]
            return Response({"School" :school})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def GetAllSchools(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            page = int(body['page'])
            itemsPerPage = int(body['per_page'])
            users = User.objects.filter(authToken=str(auth)).values('type','branchId')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
            schools = []
            schools = School.objects.filter(isActive=True).values('id',
                                            'schoolName',
                                            'schoolAddress',
                                            'creationDate',
                                            'updationDate')
            if user['type'] == "S-Admin":
                schools = []
                schools = School.objects.filter(id=int(user['branchId']),isActive=True).values('id',
                                                'schoolName',
                                                'schoolAddress',
                                                'creationDate',
                                                'updationDate')
            elif user['type'] == "B-Admin":
                schools = []
                branches = Branch.objects.filter(id=int(user['branchId'])).values('id',
                                                'branchName',
                                                'branchAddress',
                                                'creationDate',
                                                'updationDate',
                                                'schoolId')
                for branch in branches:
                    branchSchools = School.objects.filter(id=int(branch['schoolId']),isActive=True).values('id',
                                                    'schoolName',
                                                    'schoolAddress',
                                                    'creationDate',
                                                    'updationDate')
                    schools.extend(branchSchools)

            else:
                schools = School.objects.filter(isActive=True).values('id',
                                                'schoolName',
                                                'schoolAddress',
                                                'creationDate',
                                                'updationDate')
            schools = sorted(schools, key=lambda x: x['updationDate'], reverse=True)
            left = len(schools) - (page*itemsPerPage)
            next_page = True
            if left > 0:
                next_page = True
            else:
                next_page = False
            if page == -1:
                return Response({"Schools": schools, "Total" : len(schools), "NextPage" : next_page})
            else:
                return Response({"Schools": schools[(page*itemsPerPage)-itemsPerPage:page*itemsPerPage], "Total" : len(schools), "NextPage" : next_page})

        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def GetAllSchoolBranches(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            schoolId = body['school_id']
            users = User.objects.filter(authToken=str(auth)).values('type','branchId')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]

            schools = School.objects.filter(id=int(schoolId),isActive=True).values('id',
                                            'schoolName',
                                            'schoolAddress',
                                            'creationDate',
                                            'updationDate')





            if schools.count() > 0:
                school = list(schools)[0]
                allBranches = []
                allBranches = Branch.objects.filter(schoolId=int(school['id']),isActive=True).values('id',
                                                    'branchName',
                                                    'branchAddress',
                                                    'creationDate',
                                                    'updationDate',
                                                    'schoolId')

                if user['type'] == "B-Admin":
                    allBranches = []
                    allBranches = Branch.objects.filter(schoolId=int(school['id']),id=int(user['branchId']),isActive=True).values('id',
                                                    'branchName',
                                                    'branchAddress',
                                                    'creationDate',
                                                    'updationDate',
                                                    'schoolId')


                allBranches = sorted(allBranches, key=lambda x: x['updationDate'], reverse=True)
                school['branches'] = list(allBranches)
            else:
                return Response({"Error" :"No school found."})
            return Response({"School": school})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def EditSchool(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            schoolID = int(body['school_id'])
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] != "SuperAdmin":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to add schools."})
            allSchools = School.objects.filter(id=schoolID,isActive=True)
            if allSchools.count() == 0:
                return Response({"Error" :"No Schoold Found with Specified ID"})
            for i in body["SchoolData"]:
                if i == "school_name":
                    for j in allSchools:
                        j.schoolName = body["SchoolData"]['school_name']
                        j.save()
                if i == "school_address":
                    for j in allSchools:
                        j.schoolAddress = body["SchoolData"]['school_address']
                        j.save()
            schools = School.objects.filter(id=schoolID,isActive=True).values('id',
                                                            'schoolName',
                                                            'schoolAddress',
                                                            'creationDate',
                                                            'updationDate')
            school = list(schools)[0]
            return Response({"School" :school})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def RemoveSchool(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            schoolID = int(body['school_id'])
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] != "SuperAdmin":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to remive schools."})
            allSchools = School.objects.filter(id=schoolID,isActive=True)
            if allSchools.count() == 0:
                return Response({"Error" :"No Schoold Found with Specified ID"})
            for j in allSchools:
                j.isActive = False
                j.save()
            allBranches = Branch.objects.filter(schoolId=schoolID,isActive=True)
            for j in allBranches:
                j.isActive = False
                j.save()
            return Response({"Status" :"School has been removed."})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def GetDetailsOfSchool(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            schoolId = body['school_id']
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] == "BranchAdmin" or user['type'] == "Teacher" or user['type'] == "Parent":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to view branches."})
            schools = School.objects.filter(id=int(schoolId),isActive=True).values('id',
                                            'schoolName',
                                            'schoolAddress',
                                            'creationDate',
                                            'updationDate')
            if schools.count() > 0:
                school = list(schools)[0]
                allBranches = Branch.objects.filter(schoolId=int(school['id']),isActive=True).values('id',
                                                    'branchName',
                                                    'branchAddress',
                                                    'creationDate',
                                                    'updationDate',
                                                    'schoolId')
                allBranches = sorted(allBranches, key=lambda x: x['updationDate'], reverse=True)
                school['branches'] = list(allBranches)
            else:
                return Response({"Error" :"No school found."})
            return Response({"School": school})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})
