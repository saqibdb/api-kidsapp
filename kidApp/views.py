import base64
from rest_framework.response import Response
from rest_framework.decorators import api_view
from kidApp.models import User, School, Branch, Class, TeacherClass, StudentParent, Post, Comment, Like, Media, Tag
import uuid
import random
import json
from KidsApp.settings import MEDIA_URL, MEDIA_ROOT


from push_notifications.fields import hex_re, UNSIGNED_64BIT_INT_MAX_VALUE
from push_notifications.models import APNSDevice, GCMDevice, WNSDevice, WebPushDevice
from push_notifications.settings import PUSH_NOTIFICATIONS_SETTINGS as SETTINGS
from apns2.client import APNsClient
from apns2.payload import Payload
from kidApp.push import fcm_send_notification, send_apns_notification, send_apns_to_all, fcm_send_all

@api_view(['POST', ])
def GetAllAPNsRegistrations(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            title = body['push_title']
            devices = APNSDevice.objects.filter(active=True)
            allDevices = []
            for device in devices:
                allDevices.append(device.registration_id)
            #send_apns_notification(42, str(title))
            #statusFCM = fcm_send_notification(title=str(title), body="Hello World!")

            return Response({"APNSDevice" :list(allDevices)})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def SendAPNSToAll(request):
    if request.method == "POST":
        try:
            send_apns_to_all("TEST NOTIFICATION")
            #send_apns_notification([53], 1, "TEST NOTIFICATION")
            return Response({"APNSDevice" :"GOOD ONE"})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def SendFCMToAll(request):
    if request.method == "POST":

        try:
            #statusFCM = fcm_send_all(title="TEST NOTIFICATION", body="")
            statusFCM = fcm_send_notification([53], 1 ,title="TEST NOTIFICATION", body="")

            return Response({"FCMDevice" :"GOOD ONE"})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})




@api_view(['POST', ])
def RegisterDeviceForAPNs(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            authToken = body['auth_token']
            applicationId = "com.ibuildx.Efflorescence"
            registrationId = body['registration_id']
            deviceId = body['device_id']
            active = True

            users = User.objects.filter(authToken=str(authToken)).values('id','type','userName')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            user = list(users)[0]
            name = user['userName']

            devices = APNSDevice.objects.filter(device_id=str(deviceId))
            if devices.count() > 0:
                device = list(devices)[0]
                device.registration_id = registrationId
                authUsers = User.objects.filter(authToken=str(authToken))
                device.user = list(authUsers)[0]
                device.save()
            else:
                authUsers = User.objects.filter(authToken=str(authToken))
                newDevice = APNSDevice(name=str(name),application_id=str(applicationId),registration_id=str(registrationId),device_id=str(deviceId),active=active,user=list(authUsers)[0])
                newDevice.save()

            allDevices = APNSDevice.objects.filter(device_id=str(deviceId)).values('device_id',
                                                            'user',
                                                            'active')
            return Response({"APNDevices" :list(allDevices)})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def UnRegisterDeviceForAPNs(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            deviceId = body['device_id']
            devices = APNSDevice.objects.filter(device_id=str(deviceId))
            if devices.count() == 0:
                return Response({"Status" :"No Device with specified Device_ID"})
            else:
                device = list(devices)[0]
                device.delete()
            return Response({"Status" :"Device Has been Removed."})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def RegisterDeviceForFCMs(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            authToken = body['auth_token']
            applicationId = "com.umer.Efflorescence"
            registrationId = body['registration_id']
            deviceId = body['device_id']
            active = True

            users = User.objects.filter(authToken=str(authToken)).values('id','type','userName')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            user = list(users)[0]
            name = user['userName']

            devices = GCMDevice.objects.filter(device_id=str(deviceId))
            if devices.count() > 0:
                device = list(devices)[0]
                device.registration_id = registrationId
                authUsers = User.objects.filter(authToken=str(authToken))
                device.user = list(authUsers)[0]
                device.save()
            else:
                authUsers = User.objects.filter(authToken=str(authToken))
                newDevice = GCMDevice(name=str(name),application_id=str(applicationId),registration_id=str(registrationId),device_id=str(deviceId),active=active,user=list(authUsers)[0])
                newDevice.save()

            allDevices = GCMDevice.objects.filter(device_id=str(deviceId)).values('device_id',
                                                            'user',
                                                            'active')
            return Response({"GCMDevices" :list(allDevices)})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def UnRegisterDeviceForFCMs(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            deviceId = body['device_id']
            devices = GCMDevice.objects.filter(device_id=str(deviceId))
            if devices.count() == 0:
                return Response({"Status" :"No Device with specified Device_ID"})
            else:
                device = list(devices)[0]
                device.delete()
            return Response({"Status" :"Device Has been Removed."})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def EnableDisableNotificationForAccount(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            isNotification = body["is_notification"]
            users = User.objects.filter(authToken=str(auth))
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            for user in users:
                user.isNotification = isNotification
                user.save()
                appleDevices = APNSDevice.objects.filter(user=int(user.id))
                for appleDevice in appleDevices:
                    appleDevice.active = isNotification
                    appleDevice.save()
                googleDevices = GCMDevice.objects.filter(user=int(user.id))
                for googleDevice in googleDevices:
                    googleDevice.active = isNotification
                    googleDevice.save()
            users = User.objects.filter(authToken=str(auth)).values('id',
                                                            'userName',
                                                            'email',
                                                            'type',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'profilePictureURL',
                                                            'isNotification')
            return Response({"User" :list(users)[0]})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})
