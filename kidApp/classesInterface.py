import base64
from rest_framework.response import Response
from rest_framework.decorators import api_view
from kidApp.models import User, School, Branch, Class, TeacherClass, StudentParent, StudentClass, SubjectClass ,SubjectPost , Post, Comment, Like, Media, Tag, Subject
import uuid
import random
import json
from KidsApp.settings import MEDIA_URL, MEDIA_ROOT
import datetime

@api_view(['POST', ])
def AddClass(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            className = body['class_name']
            branchId = body['branch_id']
            classSubjects = body['class_subjects']
            studentsArray = body['students']

            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] == "Teacher" or user['type'] == "Parent":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to add branches."})
            allClasses = Class.objects.filter(className=str(className),branchId=int(branchId),isActive=True)
            if allClasses.count() > 0:
                return Response({"Error" :"There is already a Class present with this name and subjects"})
            branches = Branch.objects.filter(id=int(branchId)).values('id',
                                            'branchName',
                                            'branchAddress',
                                            'creationDate',
                                            'updationDate',
                                            'schoolId')
            if branches.count() == 0:
                return Response({"Error" :"No Branch Found with the specified ID"})
            new = Class(className=str(className),branchId=int(branchId))
            new.save()

            for classSubjectObj in classSubjects:
                subjectName = classSubjectObj['subject_name']
                newSubject = Subject(subjectName=str(subjectName))
                newSubject.save()
            classes = Class.objects.filter(className=str(className),branchId=int(branchId)).values('id',
                                                            'className',
                                                            'creationDate',
                                                            'updationDate')
            classe = list(classes)[0]
            branch = list(branches)[0]
            classe['branch'] = branch

            allSubjects = []
            subjectClasses = SubjectClass.objects.filter(classId=int(classe['id'])).values('id',
                                                            'subjectId')
            for subjectClasse in subjectClasses:
                subjects = Subject.objects.filter(id=int(subjectClasse['subjectId'])).values('id',
                                                                'subjectName')
                allSubjects.extend(subjects)
            classe['subjects'] = allSubjects


            classId = classe['id']
            for studentObj in studentsArray:
                studentId = studentObj['student_id']
                studentUsers = User.objects.filter(id=int(studentId)).values('type')
                if studentUsers.count() == 0:
                    return Response({"Error" :"No User Found With the specified id."})
                else:
                    studentUser = list(studentUsers)[0]
                    if studentUser['type'] != "Student":
                        return Response({"Error" :"Specified User is not Student."})
                    else:
                        studentClasses = StudentClass.objects.filter(classId=int(classId),studentId=int(studentId))
                        if studentClasses.count() == 0:
                            new = StudentClass(classId=int(classId),studentId=int(studentId))
                            new.save()
            allClassStudents = []
            studentClasses = StudentClass.objects.filter(classId=int(classe['id'])).values('id',
                                                            'classId',
                                                            'studentId')
            for studentClasse in studentClasses:
                users = User.objects.filter(id=int(studentClasse['studentId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'isNotification',
                                                                'profilePictureURL',
                                                                'deviceToken',
                                                                'type',
                                                                'studentIdentification',
                                                                'creationDate',
                                                                'updationDate',
                                                                'lastLoginDate')
                if users.count() > 0:
                    allClassStudents.append(list(users)[0])
            classe['students'] = allClassStudents


            return Response({"Class" :classe})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def AddClassWithSubjectsIds(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            className = body['class_name']
            branchId = body['branch_id']
            classSubjects = body['class_subjects']
            studentsArray = body['students']

            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] == "Teacher" or user['type'] == "Parent":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to add branches."})
            allClasses = Class.objects.filter(className=str(className),branchId=int(branchId),isActive=True)
            if allClasses.count() > 0:
                return Response({"Error" :"There is already a Class present with this name and subjects"})
            branches = Branch.objects.filter(id=int(branchId)).values('id',
                                            'branchName',
                                            'branchAddress',
                                            'creationDate',
                                            'updationDate',
                                            'schoolId')
            if branches.count() == 0:
                return Response({"Error" :"No Branch Found with the specified ID"})
            new = Class(className=str(className),branchId=int(branchId))
            new.save()

            for classSubjectObj in classSubjects:
                subjectId = classSubjectObj['subject_id']
                newSubject = SubjectClass(classId=int(new.id),subjectId=int(subjectId))
                newSubject.save()
            classes = Class.objects.filter(className=str(className),branchId=int(branchId)).values('id',
                                                            'className',
                                                            'creationDate',
                                                            'updationDate')
            classe = list(classes)[0]
            branch = list(branches)[0]
            classe['branch'] = branch
            allSubjects = []
            subjectClasses = SubjectClass.objects.filter(classId=int(classe['id'])).values('id',
                                                            'subjectId')
            for subjectClasse in subjectClasses:
                subjects = Subject.objects.filter(id=int(subjectClasse['subjectId'])).values('id',
                                                                'subjectName')
                allSubjects.extend(subjects)
            classe['subjects'] = allSubjects
            classId = classe['id']
            for studentObj in studentsArray:
                studentId = studentObj['student_id']
                studentUsers = User.objects.filter(id=int(studentId)).values('type')
                if studentUsers.count() == 0:
                    return Response({"Error" :"No User Found With the specified id."})
                else:
                    studentUser = list(studentUsers)[0]
                    if studentUser['type'] != "Student":
                        return Response({"Error" :"Specified User is not Student."})
                    else:
                        studentClasses = StudentClass.objects.filter(classId=int(classId),studentId=int(studentId))
                        if studentClasses.count() == 0:
                            new = StudentClass(classId=int(classId),studentId=int(studentId))
                            new.save()
            allClassStudents = []
            studentClasses = StudentClass.objects.filter(classId=int(classe['id'])).values('id',
                                                            'classId',
                                                            'studentId')
            for studentClasse in studentClasses:
                users = User.objects.filter(id=int(studentClasse['studentId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'isNotification',
                                                                'profilePictureURL',
                                                                'deviceToken',
                                                                'type',
                                                                'studentIdentification',
                                                                'creationDate',
                                                                'updationDate',
                                                                'lastLoginDate')
                if users.count() > 0:
                    allClassStudents.append(list(users)[0])
            classe['students'] = allClassStudents


            return Response({"Class" :classe})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})








@api_view(['POST', ])
def GetAllClasses(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            page = int(body['page'])
            itemsPerPage = int(body['per_page'])
            users = User.objects.filter(authToken=str(auth)).values('type','branchId')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]

            classes = Class.objects.filter(isActive=True).values('id',
                                            'className',
                                            'creationDate',
                                            'updationDate',
                                            'branchId')
            if user['type'] == "S-Admin":
                classes = []
                schools = School.objects.filter(id=int(user['branchId'])).values('id',
                                                'schoolName',
                                                'schoolAddress',
                                                'creationDate',
                                                'updationDate')
                for school in schools:
                    branches = Branch.objects.filter(schoolId=int(school['id'])).values('id',
                                                    'branchName',
                                                    'branchAddress',
                                                    'creationDate',
                                                    'updationDate',
                                                    'schoolId')
                    for branch in branches:
                        branchClasses = Class.objects.filter(isActive=True,branchId=int(branch['id'])).values('id',
                                                        'className',
                                                        'creationDate',
                                                        'updationDate',
                                                        'branchId')
                        classes.extend(branchClasses)


            if user['type'] == "B-Admin":
                classes = []
                branches = Branch.objects.filter(id=int(user['branchId'])).values('id',
                                                'branchName',
                                                'branchAddress',
                                                'creationDate',
                                                'updationDate',
                                                'schoolId')
                for branch in branches:
                    branchClasses = Class.objects.filter(isActive=True,branchId=int(branch['id'])).values('id',
                                                    'className',
                                                    'creationDate',
                                                    'updationDate',
                                                    'branchId')
                    classes.extend(branchClasses)



            allClassesWithBranches = []
            for classe in classes:
                allClassTeachers = []
                teacherClasses = TeacherClass.objects.filter(classId=int(classe['id'])).values('id',
                                                                'classId',
                                                                'teacherId',
                                                                'isClassTeacher')
                for teacherClasse in teacherClasses:
                    users = User.objects.filter(id=int(teacherClasse['teacherId'])).values('id',
                                                                    'userName',
                                                                    'email',
                                                                    'age',
                                                                    'phone',
                                                                    'gender',
                                                                    'isNotification',
                                                                    'profilePictureURL',
                                                                    'deviceToken',
                                                                    'type',
                                                                    'studentIdentification',
                                                                    'creationDate',
                                                                    'updationDate',
                                                                    'lastLoginDate')
                    for user in users:
                        user['isClassTeacher'] = teacherClasse['isClassTeacher']
                    if users.count() > 0:
                        allClassTeachers.append(list(users)[0])
                classe['teachers'] = allClassTeachers
                allSubjects = []
                subjectClasses = SubjectClass.objects.filter(classId=int(classe['id'])).values('id',
                                                                'subjectId')
                for subjectClasse in subjectClasses:
                    subjects = Subject.objects.filter(id=int(subjectClasse['subjectId']),isActive=True).values('id',
                                                                    'subjectName')
                    allSubjects.extend(subjects)
                classe['subjects'] = allSubjects

                branches = Branch.objects.filter(id=int(classe['branchId'])).values('id',
                                                'branchName',
                                                'branchAddress',
                                                'creationDate',
                                                'updationDate',
                                                'schoolId')
                if branches.count() > 0:
                    branch = list(branches)[0]
                    schools = School.objects.filter(id=int(branch['schoolId'])).values('id',
                                                    'schoolName',
                                                    'schoolAddress',
                                                    'creationDate',
                                                    'updationDate')
                    if schools.count() > 0:
                        branch['school'] = list(schools)[0]
                    classe['branch'] = branch
                    allClassesWithBranches.append(classe)

            if 'filters' in body:
                filters = body['filters']
                for filterObj in filters:
                    if filterObj['filter_name'] == "school":
                        schoolId = int(filterObj['filter_value'])
                        for classe in list(allClassesWithBranches):
                            if classe['branch']['school']['id'] != schoolId:
                                allClassesWithBranches.remove(classe)
                    if filterObj['filter_name'] == "branch":
                        branchId = int(filterObj['filter_value'])
                        for classe in list(allClassesWithBranches):
                            if classe['branch']['id'] != branchId:
                                allClassesWithBranches.remove(classe)

            allClassesWithBranches = sorted(allClassesWithBranches, key=lambda x: x['updationDate'], reverse=True)
            left = len(allClassesWithBranches) - (page*itemsPerPage)
            next_page = True
            if left > 0:
                next_page = True
            else:
                next_page = False
            if page == -1:
                return Response({"Classes": allClassesWithBranches, "Total" : len(allClassesWithBranches), "NextPage" : next_page})
            else:
                return Response({"Classes": allClassesWithBranches[(page*itemsPerPage)-itemsPerPage:page*itemsPerPage], "Total" : len(allClassesWithBranches), "NextPage" : next_page})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})

@api_view(['POST', ])
def GetAllBranchClasses(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            branchId = int(body['branch_id'])
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] == "Teacher" or user['type'] == "Parent":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to view classes."})

            branches = Branch.objects.filter(id=int(branchId)).values('id',
                                            'branchName',
                                            'branchAddress',
                                            'creationDate',
                                            'updationDate',
                                            'schoolId')

            for branch in branches:
                schools = School.objects.filter(id=int(branch['schoolId'])).values('id',
                                                'schoolName',
                                                'schoolAddress',
                                                'creationDate',
                                                'updationDate')
                if schools.count() > 0:
                    branch['school'] = list(schools)[0]

            allClassesWithBranches = []
            if branches.count() > 0:

                classes = Class.objects.filter(branchId=int(branchId),isActive=True).values('id',
                                                'className',
                                                'creationDate',
                                                'updationDate',
                                                'branchId')

                for classe in classes:
                    allClassTeachers = []
                    teacherClasses = TeacherClass.objects.filter(classId=int(classe['id'])).values('id',
                                                                    'classId',
                                                                    'teacherId',
                                                                    'isClassTeacher')
                    for teacherClasse in teacherClasses:
                        users = User.objects.filter(id=int(teacherClasse['teacherId'])).values('id',
                                                                        'userName',
                                                                        'email',
                                                                        'age',
                                                                        'phone',
                                                                        'gender',
                                                                        'isNotification',
                                                                        'profilePictureURL',
                                                                        'deviceToken',
                                                                        'type',
                                                                        'studentIdentification',
                                                                        'creationDate',
                                                                        'updationDate',
                                                                        'lastLoginDate')
                        for user in users:
                            user['isClassTeacher'] = teacherClasse['isClassTeacher']
                        if users.count() > 0:
                            allClassTeachers.append(list(users)[0])
                    classe['teachers'] = allClassTeachers

                    allSubjects = []
                    subjectClasses = SubjectClass.objects.filter(classId=int(classe['id'])).values('id',
                                                                    'subjectId')
                    for subjectClasse in subjectClasses:
                        subjects = Subject.objects.filter(id=int(subjectClasse['subjectId'])).values('id',
                                                                        'subjectName')
                        allSubjects.extend(subjects)
                    classe['subjects'] = allSubjects


                    classe['branch'] = list(branches)[0]
                    allClassesWithBranches.append(classe)
            else:
                return Response({"Error" :"No branch found."})

            allClassesWithBranches = sorted(allClassesWithBranches, key=lambda x: x['updationDate'], reverse=True)
            return Response({"Classes": allClassesWithBranches, "Total" : len(allClassesWithBranches)})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})





@api_view(['POST', ])
def GetAllTeacherClasses(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            users = User.objects.filter(authToken=str(auth)).values('id','type','branchId')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] == "Student":
                    return Response({"Error" :"You are Not Teacher. Only Teacher can view their assigned classes."})


                allClassesWithBranches = []
                allClasses = []
                if user['type'] == "Teacher":
                    teacherClasses = TeacherClass.objects.filter(teacherId=int(user['id'])).values('id',
                                                'classId',
                                                'teacherId',
                                                'isClassTeacher')
                    for teacherClass in teacherClasses:
                        classes = Class.objects.filter(id=int(teacherClass['classId'])).values('id',
                                                    'className',
                                                    'creationDate',
                                                    'updationDate',
                                                    'branchId')
                        for classe in classes:
                            classe['isClassTeacher'] = teacherClass['isClassTeacher']
                        allClasses.extend(classes)



                if user['type'] == "SuperAdmin":
                    classes = Class.objects.values('id',
                                                    'className',
                                                    'creationDate',
                                                    'updationDate',
                                                    'branchId')
                    allClasses.extend(classes)

                if user['type'] == "B-Admin":
                    classes = Class.objects.filter(branchId=int(user['branchId']), isActive=True).values('id',
                                                   'className',
                                                   'creationDate',
                                                   'updationDate',
                                                   'branchId')
                    allClasses.extend(classes)

                if user['type'] == "S-Admin":
                    allBranches = Branch.objects.filter(schoolId=int(user['branchId']), isActive=True).values('id')
                    for branch in allBranches:
                        classes = Class.objects.filter(branchId=int(branch['id']), isActive=True).values('id',
                                                                                                             'className',
                                                                                                             'creationDate',
                                                                                                             'updationDate',
                                                                                                             'branchId')
                        allClasses.extend(classes)

                if user['type'] == "Parent":
                    studentParents = StudentParent.objects.filter(parentId=int(user['id'])).values('studentId',
                                                                                                 'parentId')
                    allChildren = []
                    for studentParent in studentParents:
                        childUsers = User.objects.filter(id=int(studentParent['studentId']), isActive=True).values('id','userName')
                        allChildren.extend(childUsers)
                    for children in allChildren:
                        studentClasses = StudentClass.objects.filter(studentId=int(children['id'])).values('studentId',
                                                                                                       'classId')
                        for studentClasse in studentClasses:
                            classes = Class.objects.filter(id=int(studentClasse['classId']), isActive=True).values('id',
                                                                                                             'className',
                                                                                                             'creationDate',
                                                                                                             'updationDate',
                                                                                                             'branchId')
                            for classe in classes:
                                classe['className'] = classe['className']+" ("+children['userName']+")"
                                allClasses.append(classe)




                for classe in allClasses:
                    branches = Branch.objects.filter(id=int(classe['branchId'])).values('id',
                                                        'branchName',
                                                        'branchAddress',
                                                        'schoolId')
                    if branches.count() > 0:
                        branch = list(branches)[0]
                        schools = School.objects.filter(id=int(branch['schoolId'])).values('id',
                                                            'schoolName',
                                                            'schoolAddress')
                        if schools.count() > 0:
                            branch['school'] = list(schools)[0]
                        classe['branch'] = branch
                        allSubjects = []
                        subjectClasses = SubjectClass.objects.filter(classId=int(classe['id'])).values('id',
                                                                        'subjectId')
                        for subjectClasse in subjectClasses:
                            subjects = Subject.objects.filter(id=int(subjectClasse['subjectId'])).values('id',
                                                                            'subjectName')
                            allSubjects.extend(subjects)
                        classe['subjects'] = allSubjects
                        if classe not in allClassesWithBranches:
                            allClassesWithBranches.append(classe)
                return Response({"Classes": allClassesWithBranches, "Total" : len(allClassesWithBranches)})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})




@api_view(['POST', ])
def AddSubject(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            subjectName = body['subject_name']
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] == "Parent":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to add subjects."})

            #subjects = Subject.objects.filter(subjectName=str(subjectName),isActive=True)
            #if subjects.count() == 0:
                #return Response({"Error" :"A Subject with this name already exists."})
            newSubject = Subject(subjectName=str(subjectName))
            newSubject.save()
            return Response({"Status" :"New subject added"})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def GetAllSubjects(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            page = -1
            itemsPerPage = 10
            if 'page' in body:
                page = int(body['page'])
            if 'per_page' in body:
                itemsPerPage = int(body['per_page'])
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
            allSubject = Subject.objects.filter(isActive=True).values('id',
                                        'subjectName',
                                        'creationDate',
                                        'updationDate')
            allSubject = sorted(allSubject, key=lambda x: x['updationDate'], reverse=True)

            left = len(allSubject) - (page*itemsPerPage)
            next_page = True
            if left > 0:
                next_page = True
            else:
                next_page = False
            if page == -1:
                return Response({"Subjects": allSubject, "Total" : len(allSubject), "NextPage" : next_page})
            else:
                return Response({"Subjects": allSubject[(page*itemsPerPage)-itemsPerPage:page*itemsPerPage], "Total" : len(allSubject), "NextPage" : next_page})

        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})




@api_view(['POST', ])
def EditSubject(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            subjectId = body['subject_id']
            subjectName = body['subject_name']
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] == "Parent":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to add subjects."})
            allSubjects = Subject.objects.filter(id=int(subjectId),isActive=True)
            for subject in allSubjects:
                subject.subjectName = str(subjectName)
                subject.updationDate = datetime.datetime.now()
                subject.save()
            subjects = Subject.objects.filter(id=int(subjectId),isActive=True).values('id',
                                        'subjectName')
            return Response({"Edited Subject" :list(subjects)[0]})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def RemoveSubject(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            subjectId = body['subject_id']
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] == "Parent":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to add subjects."})
            allSubjects = Subject.objects.filter(id=int(subjectId),isActive=True)
            for subject in allSubjects:
                subject.isActive = False
                subject.save()
                allSubjectClasses = SubjectClass.objects.filter(subjectId=int(subject.id))
                for subjectClasse in allSubjectClasses:
                    subjectClasse.delete()
                allSubjectPosts = SubjectPost.objects.filter(subjectId=int(subject.id))
                for subjectPost in allSubjectPosts:
                    subjectPost.delete()
            return Response({"Status" :"Subject has been removed."})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def GetDetailsSubject(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            subjectId = body['subject_id']
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
            allSubject = Subject.objects.filter(id=int(subjectId),isActive=True).values('id',
                                        'subjectName')
            if allSubject.count() == 0:
                return Response({"Subjects": "No Subject found with speciefied id."})
            else:
                return Response({"Subjects": list(allSubject)[0]})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def EditClass(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            classID = int(body['class_id'])
            users = User.objects.filter(authToken=str(auth)).values('type','id')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                
            alreadyClass = Class.objects.filter(id=int(classID),isActive=True)
            if alreadyClass.count() == 0:
                return Response({"Error" :"No Class Found with Specified ID"})
            for i in body["ClassData"]:
                if i == "class_name":
                    for j in alreadyClass:
                        j.className = str(body["ClassData"]['class_name'])
                        j.save()
                if i == "branch_id":
                    for j in alreadyClass:
                        j.branchId = int(body["ClassData"]['branch_id'])
                        j.save()

                if i == "class_subjects":
                    subjectClasses = SubjectClass.objects.filter(classId=int(classID))
                    for subjectClasse in subjectClasses:
                        subjectClasse.delete()
                    classSubjects = body["ClassData"]['class_subjects']

                    for classSubjectObj in classSubjects:
                        subjectId = classSubjectObj['subject_id']
                        newSubject = SubjectClass(classId=int(classID),subjectId=int(subjectId))
                        newSubject.save()

                if i == "students":
                    studentClasses = StudentClass.objects.filter(classId=int(classID))
                    for studentClasse in studentClasses:
                        studentClasse.delete()

                    studentsArray = body["ClassData"]['students']
                    for studentObj in studentsArray:
                        studentId = studentObj['student_id']
                        studentUsers = User.objects.filter(id=int(studentId)).values('type')
                        if studentUsers.count() > 0:
                            studentUser = list(studentUsers)[0]
                            if studentUser['type'] == "Student":
                                studentClasses = StudentClass.objects.filter(classId=int(classID),studentId=int(studentId))
                                if studentClasses.count() == 0:
                                    new = StudentClass(classId=int(classID),studentId=int(studentId))
                                    new.save()

            classes = Class.objects.filter(id=int(classID),isActive=True).values('id',
                                                            'className',
                                                            'creationDate',
                                                            'updationDate',
                                                            'branchId')
            classe = list(classes)[0]

            branches = Branch.objects.filter(id=int(classe['branchId']),isActive=True).values('id',
                                            'branchName',
                                            'branchAddress',
                                            'creationDate',
                                            'updationDate',
                                            'schoolId')
            branch = list(branches)[0]
            classe['branch'] = branch
            allSubjects = []
            subjectClasses = SubjectClass.objects.filter(classId=int(classe['id'])).values('id',
                                                            'subjectId')
            for subjectClasse in subjectClasses:
                subjects = Subject.objects.filter(id=int(subjectClasse['subjectId']),isActive=True).values('id',
                                                                'subjectName')
                allSubjects.extend(subjects)
            classe['subjects'] = allSubjects
            allClassStudents = []
            studentClasses = StudentClass.objects.filter(classId=int(classe['id'])).values('id',
                                                            'classId',
                                                            'studentId')
            for studentClasse in studentClasses:
                users = User.objects.filter(id=int(studentClasse['studentId']),isActive=True).values('id',
                                                                'userName',
                                                                'email',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'isNotification',
                                                                'profilePictureURL',
                                                                'deviceToken',
                                                                'type',
                                                                'studentIdentification',
                                                                'creationDate',
                                                                'updationDate',
                                                                'lastLoginDate')
                if users.count() > 0:
                    allClassStudents.append(list(users)[0])
            classe['students'] = allClassStudents
            return Response({"EditedClass" :classe})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def RemoveClass(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            classID = int(body['class_id'])
            users = User.objects.filter(authToken=str(auth)).values('type','id')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] != "SuperAdmin":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to remove class."})
            alreadyClass = Class.objects.filter(id=int(classID),isActive=True)
            if alreadyClass.count() == 0:
                return Response({"Error" :"No Class Found with Specified ID"})
            for j in alreadyClass:
                j.isActive = False
                j.save()
            subjectClasses = SubjectClass.objects.filter(classId=int(classID))
            for subjectClasse in subjectClasses:
                subjectClasse.delete()
            studentClasses = StudentClass.objects.filter(classId=int(classID))
            for studentClasse in studentClasses:
                studentClasse.delete()
            return Response({"Status" :"Class has been removed."})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def GetDetialsOfClass(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            classID = int(body['class_id'])
            users = User.objects.filter(authToken=str(auth)).values('type','id')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            classes = Class.objects.filter(id=int(classID),isActive=True).values('id',
                                                            'className',
                                                            'creationDate',
                                                            'updationDate',
                                                            'branchId')
            classe = list(classes)[0]

            branches = Branch.objects.filter(id=int(classe['branchId']),isActive=True).values('id',
                                            'branchName',
                                            'branchAddress',
                                            'creationDate',
                                            'updationDate',
                                            'schoolId')
            if branches.count() > 0:
                branch = list(branches)[0]
                schools = School.objects.filter(id=int(branch['schoolId'])).values('id',
                                                'schoolName',
                                                'schoolAddress',
                                                'creationDate',
                                                'updationDate')
                if schools.count() > 0:
                    branch['school'] = list(schools)[0]
            branch = list(branches)[0]
            classe['branch'] = branch
            allSubjects = []
            subjectClasses = SubjectClass.objects.filter(classId=int(classe['id'])).values('id',
                                                            'subjectId')
            for subjectClasse in subjectClasses:
                subjects = Subject.objects.filter(id=int(subjectClasse['subjectId']),isActive=True).values('id',
                                                                'subjectName')
                allSubjects.extend(subjects)
            classe['subjects'] = allSubjects
            allClassStudents = []
            studentClasses = StudentClass.objects.filter(classId=int(classe['id'])).values('id',
                                                            'classId',
                                                            'studentId')
            for studentClasse in studentClasses:
                users = User.objects.filter(id=int(studentClasse['studentId']),isActive=True).values('id',
                                                                'userName',
                                                                'email',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'isNotification',
                                                                'profilePictureURL',
                                                                'deviceToken',
                                                                'type',
                                                                'studentIdentification',
                                                                'creationDate',
                                                                'updationDate',
                                                                'lastLoginDate')
                if users.count() > 0:
                    allClassStudents.append(list(users)[0])
            classe['students'] = allClassStudents
            return Response({"Class" :classe})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})
