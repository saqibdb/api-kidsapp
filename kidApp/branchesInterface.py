import base64
from rest_framework.response import Response
from rest_framework.decorators import api_view
from kidApp.models import User, School, Branch, Class, TeacherClass, StudentParent, Post, Comment, Like, Media, Tag
import uuid
import random
import json
from KidsApp.settings import MEDIA_URL, MEDIA_ROOT



@api_view(['POST', ])
def AddBranch(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            branchName = body['branch_name']
            branchAddress = body['branch_address']
            schoolId = body['school_id']
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] == "BranchAdmin" or user['type'] == "Teacher" or user['type'] == "Parent":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to add branches."})
            allBranches = Branch.objects.filter(branchName=str(branchName),branchAddress=str(branchAddress),schoolId=int(schoolId))
            if allBranches.count() > 0:
                return Response({"Error" :"There is already a branch present with this name and address"})
            schools = School.objects.filter(id=int(schoolId)).values('id',
                                            'schoolName',
                                            'schoolAddress',
                                            'creationDate',
                                            'updationDate')
            if schools.count() == 0:
                return Response({"Error" :"No School Found with the specified ID"})
            new = Branch(branchName=str(branchName),branchAddress=str(branchAddress),schoolId=int(schoolId))
            new.save()
            branches = Branch.objects.filter(branchName=str(branchName),branchAddress=str(branchAddress),schoolId=int(schoolId),isActive=True).values('id',
                                                            'branchName',
                                                            'branchAddress',
                                                            'creationDate',
                                                            'updationDate')
            branch = list(branches)[0]
            school = list(schools)[0]
            branch['school'] = school
            return Response({"Branch" :branch})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def GetAllBranches(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            page = int(body['page'])
            itemsPerPage = int(body['per_page'])
            users = User.objects.filter(authToken=str(auth)).values('type','branchId')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]

            allBranches = Branch.objects.filter(isActive=True).values('id',
                                                'branchName',
                                                'branchAddress',
                                                'creationDate',
                                                'updationDate',
                                                'schoolId')
            if user['type'] == "B-Admin":
                allBranches = Branch.objects.filter(id=int(user['branchId']),isActive=True).values('id',
                                                    'branchName',
                                                    'branchAddress',
                                                    'creationDate',
                                                    'updationDate',
                                                    'schoolId')
            if user['type'] == "S-Admin":
                allBranches = Branch.objects.filter(schoolId=int(user['branchId']),isActive=True).values('id',
                                                    'branchName',
                                                    'branchAddress',
                                                    'creationDate',
                                                    'updationDate',
                                                    'schoolId')

            allBranchesWithSchool = []
            for branch in allBranches:
                schools = School.objects.filter(id=int(branch['schoolId']),isActive=True).values('id',
                                                'schoolName',
                                                'schoolAddress',
                                                'creationDate',
                                                'updationDate')
                if schools.count() > 0:
                    branch['school'] = list(schools)[0]
                    allBranchesWithSchool.append(branch)
            allBranchesWithSchool = sorted(allBranchesWithSchool, key=lambda x: x['updationDate'], reverse=True)
            left = len(allBranchesWithSchool) - (page*itemsPerPage)
            next_page = True
            if left > 0:
                next_page = True
            else:
                next_page = False
            if page == -1:
                return Response({"Branches": allBranchesWithSchool, "Total" : len(allBranchesWithSchool), "NextPage" : next_page})
            else:
                return Response({"Branches": allBranchesWithSchool[(page*itemsPerPage)-itemsPerPage:page*itemsPerPage], "Total" : len(allBranchesWithSchool), "NextPage" : next_page})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})




@api_view(['POST', ])
def EditBranch(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            branchID = int(body['branch_id'])
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                
            allBranches = Branch.objects.filter(id=branchID,isActive=True)
            if allBranches.count() == 0:
                return Response({"Error" :"No Branch Found with Specified ID"})
            for i in body["BranchData"]:
                if i == "branch_name":
                    for j in allBranches:
                        j.branchName = body["BranchData"]['branch_name']
                        j.save()
                if i == "branch_address":
                    for j in allBranches:
                        j.branchAddress = body["BranchData"]['branch_address']
                        j.save()
                if i == "school_id":
                    for j in allBranches:
                        j.schoolId = int(body["BranchData"]['school_id'])
                        j.save()
            branches = Branch.objects.filter(id=branchID,isActive=True).values('id',
                                                            'branchName',
                                                            'branchAddress',
                                                            'creationDate',
                                                            'updationDate',
                                                            'schoolId')
            for branch in branches:
                schools = School.objects.filter(id=int(branch['schoolId'])).values('id',
                                                'schoolName',
                                                'schoolAddress',
                                                'creationDate',
                                                'updationDate')
                if schools.count() > 0:
                    branch['school'] = list(schools)[0]
            branch = list(branches)[0]
            return Response({"Branch" :branch})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def RemoveBranch(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            branchID = int(body['branch_id'])
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] != "SuperAdmin":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to remove branches."})
            allBranches = Branch.objects.filter(id=branchID,isActive=True)
            if allBranches.count() == 0:
                return Response({"Error" :"No branch Found with Specified ID"})
            for j in allBranches:
                j.isActive = False
                j.save()
            return Response({"Status" :"Branch has been removed."})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def GetDetialsOfBranch(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            branchID = int(body['branch_id'])
            users = User.objects.filter(authToken=str(auth)).values('type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            else:
                user = list(users)[0]
                if user['type'] == "BranchAdmin" or user['type'] == "Teacher" or user['type'] == "Parent":
                    return Response({"Error" :"User of type "+user['type']+" do not have ability to view branches."})
            allBranches = Branch.objects.filter(id=int(branchID),isActive=True).values('id',
                                                'branchName',
                                                'branchAddress',
                                                'creationDate',
                                                'updationDate',
                                                'schoolId')
            if allBranches.count() == 0:
                return Response({"Error": "No Branch Found With Specified ID."})
            allBranchesWithSchool = []
            for branch in allBranches:
                schools = School.objects.filter(id=int(branch['schoolId']),isActive=True).values('id',
                                                'schoolName',
                                                'schoolAddress',
                                                'creationDate',
                                                'updationDate')

                classes = Class.objects.filter(branchId=int(branch['id']),isActive=True).values('id',
                                                'className',
                                                'creationDate',
                                                'updationDate',
                                                'branchId')
                branch['classes'] = classes
                if schools.count() > 0:
                    branch['school'] = list(schools)[0]
                    allBranchesWithSchool.append(branch)
            return Response({"Branch": list(allBranchesWithSchool)[0]})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})
