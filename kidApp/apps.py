from django.apps import AppConfig


class KidsAppConfig(AppConfig):
    name = 'kidApp'
