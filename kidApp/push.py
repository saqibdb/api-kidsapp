from __future__ import absolute_import

import six
from django.conf import settings
from apns2.client import APNsClient, Notification
from apns2.payload import Payload
from pyfcm import FCMNotification
from rest_framework import permissions, status
from rest_framework.fields import IntegerField, CharField, empty
from rest_framework.response import Response
from rest_framework.serializers import ModelSerializer, Serializer, ValidationError, SerializerMethodField
from rest_framework.viewsets import ModelViewSet

from push_notifications.fields import hex_re, UNSIGNED_64BIT_INT_MAX_VALUE
from push_notifications.models import APNSDevice, GCMDevice, WNSDevice, WebPushDevice
from push_notifications.settings import PUSH_NOTIFICATIONS_SETTINGS as SETTINGS

from kidApp.models import User


_client = APNsClient(SETTINGS['APNS_CERTIFICATE'], use_sandbox=settings.DEBUG,
                    use_alternative_port=False)
_push_service = FCMNotification(api_key=SETTINGS['FCM_API_KEY'], json_encoder=None)

def send_apns_notification(userIds, postId , alert, device=None, use_sandbox=settings.DEBUG,
                           use_alternative_port=False, sound="default", badge=1):
    try:
        payload = Payload(alert=alert, sound=sound, badge=badge,custom={'postId': postId})
        if device is None:
            notifications = []
            devices = []
            for userId in userIds:
                devices.extend(APNSDevice.objects.filter(active=True,user=int(userId)))
            for device in devices:
                notification = Notification(token=device.registration_id, payload=payload)
                notifications.append(notification)
            _client.send_notification_batch(notifications, SETTINGS['APNS_TOPIC'])
        else:
            _client.send_notification(device.registration_id, payload, SETTINGS['APNS_TOPIC'])
        return "GOOD"
    except KeyError as e:
        return str(e)
    except ValueError as e:
        return str(e)
    except Exception as e:
        return str(e)


def send_apns_to_all( alert, device=None, use_sandbox=settings.DEBUG,
                           use_alternative_port=False, sound="default", badge=1):
    payload = Payload(alert=alert, sound=sound, badge=badge,custom={'postId': 1})
    notifications = []
    devices = []
    devices.extend(APNSDevice.objects.filter(active=True))
    for device in devices:
        notification = Notification(token=device.registration_id, payload=payload)
        notifications.append(notification)
    _client.send_notification_batch(notifications, SETTINGS['APNS_TOPIC'])



def fcm_send_all(device=None,
                          title=None,
                          body=None,
                          icon=None,
                          data=None,
                          sound=None,
                          badge=None,
                          json_encoder=None,
                          **kwargs
                          ):

    data_message = {"postId" : 1}
    registration_ids = []
    devices = []
    devices.extend(GCMDevice.objects.filter(active=True))
    for device in devices:
        registration_ids.append(device.registration_id)
    result = _push_service.notify_multiple_devices(registration_ids=registration_ids,
                                                   message_title=title,
                                                   message_body=body,
                                                   message_icon=icon,
                                                   data_message=data_message,
                                                   sound=sound,
                                                   click_action="home",
                                                   badge=badge, **kwargs)



def fcm_send_notification(userIds,
                          postId,
                          device=None,
                          title=None,
                          body=None,
                          icon=None,
                          data=None,
                          sound=None,
                          badge=None,
                          json_encoder=None,
                          **kwargs
                          ):

    data_message = {"postId" : postId,"click_action" : "home"}
    try:
        if device is None:
            registration_ids = []
            devices = []
            for userId in userIds:
                devices.extend(GCMDevice.objects.filter(active=True,user=int(userId)))
            for device in devices:
                registration_ids.append(device.registration_id)
            result = _push_service.notify_multiple_devices(registration_ids=registration_ids,
                                                           message_title=title,
                                                           message_body=body,
                                                           message_icon=icon,
                                                           data_message=data_message,
                                                           sound=sound,
                                                           click_action="home",
                                                           badge=badge, **kwargs)

        else:
            result = _push_service.notify_single_device(registration_id=device.registration_id,
                                                       message_title=title,
                                                       message_body=body,
                                                       message_icon=icon,
                                                       data_message=data_message,
                                                       sound=sound,
                                                       click_action="home",
                                                       badge=badge, **kwargs)
        return "GOOD"
    except KeyError as e:
        return str(e)
    except ValueError as e:
        return str(e)
    except Exception as e:
        return str(e)


def auth_token_required(value):
    if not value:
        raise ValidationError("auth_token is required")
    return value


class AuthCharField(CharField):

    def run_validation(self, data=''):
        if data == empty:
            data = ""
        value = self.to_internal_value(data)
        self.run_validators(value)
        return value


# Fields
class HexIntegerField(IntegerField):
    """Store an integer represented as a hex string of form "0x01".
    """

    def to_internal_value(self, data):
        # validate hex string and convert it to the unsigned
        # integer representation for internal use
        try:
            data = int(data, 16) if type(data) != int else data
        except ValueError:
            raise ValidationError("Device ID is not a valid hex number")
        return super(HexIntegerField, self).to_internal_value(data)

    def to_representation(self, value):
        return value


# Serializers
class DeviceSerializerMixin(ModelSerializer):

    class Meta:
        fields = (
            "id", "name", "application_id", "registration_id", "device_id",
            "active", "date_created"
        )
        read_only_fields = ("date_created",)

        # See https://github.com/tomchristie/django-rest-framework/issues/1101
        extra_kwargs = {"active": {"default": True}}


class APNSDeviceSerializer(ModelSerializer):

    auth_token = AuthCharField(
        max_length=1000,
        required=False,
        validators=[auth_token_required],
        help_text="This field is required"
    )

    class Meta:
        model = APNSDevice
        fields = (
            "id", "name", "application_id", "registration_id", "device_id",
            "active", "date_created", "auth_token"
        )
        read_only_fields = ("date_created",)

        # See https://github.com/tomchristie/django-rest-framework/issues/1101
        extra_kwargs = {"active": {"default": True}}

    def validate_registration_id(self, value):
        # iOS device tokens are 256-bit hexadecimal (64 characters). In 2016 Apple is increasing
        # iOS device tokens to 100 bytes hexadecimal (200 characters).

        if hex_re.match(value) is None or len(value) not in (64, 200):
            raise ValidationError("Registration ID (device token) is invalid")

        return value

    def validate_auth_token(self, value):
        # device ids are 64 bit unsigned values
        if not value:
            raise ValidationError("auth_token is required")
        try:
            self.auth_user = User.objects.filter(authToken=value)[0]
        except IndexError:
            raise ValidationError("Invalid auth token")
        return value

    def save(self, **kwargs):
        self.validated_data.pop('auth_token', None)
        kwargs['user'] = self.auth_user
        return super().save(**kwargs)


class UniqueRegistrationSerializerMixin(Serializer):

    def validate(self, attrs):
        devices = None
        primary_key = None
        request_method = None

        if self.initial_data.get("registration_id", None):
            if self.instance:
                request_method = "update"
                primary_key = self.instance.id
            else:
                request_method = "create"
        else:
            if self.context["request"].method in ["PUT", "PATCH"]:
                request_method = "update"
                primary_key = self.instance.id
            elif self.context["request"].method == "POST":
                request_method = "create"

        Device = self.Meta.model
        if request_method == "update":
            reg_id = attrs.get("registration_id", self.instance.registration_id)
            devices = Device.objects.filter(registration_id=reg_id).exclude(id=primary_key)
        elif request_method == "create":
            devices = Device.objects.filter(registration_id=attrs["registration_id"])

        if devices:
            raise ValidationError({"registration_id": "This field must be unique."})
        return attrs


class GCMDeviceSerializer(UniqueRegistrationSerializerMixin, ModelSerializer):

    device_id = HexIntegerField(
        help_text="ANDROID_ID / TelephonyManager.getDeviceId() (e.g: 0x01)",
        style={"input_type": "text"},
        required=False,
        allow_null=True
    )

    auth_token = AuthCharField(
        max_length=1000,
        required=False,
        validators=[auth_token_required],
        help_text="This field is required"
    )

    class Meta(DeviceSerializerMixin.Meta):
        model = GCMDevice
        fields = (
            "name", "registration_id", "device_id", "active", "date_created",
            "application_id", "auth_token")
        extra_kwargs = {"id": {"read_only": False, "required": False}}

    def validate_device_id(self, value):
        # device ids are 64 bit unsigned values
        if value and value > UNSIGNED_64BIT_INT_MAX_VALUE:
            raise ValidationError("Device ID is out of range")
        return value

    def validate_auth_token(self, value):
        # device ids are 64 bit unsigned values
        if not value:
            raise ValidationError("auth_token is required")
        try:
            self.auth_user = User.objects.filter(authToken=value)[0]
        except IndexError:
            raise ValidationError("Invalid auth token")
        return value

    def save(self, **kwargs):
        self.validated_data.pop('auth_token', None)
        kwargs['user'] = self.auth_user
        kwargs['cloud_message_type'] = 'FCM'
        return super().save(**kwargs)


class WNSDeviceSerializer(UniqueRegistrationSerializerMixin, ModelSerializer):
	class Meta(DeviceSerializerMixin.Meta):
		model = WNSDevice


class WebPushDeviceSerializer(UniqueRegistrationSerializerMixin, ModelSerializer):
	class Meta(DeviceSerializerMixin.Meta):
		model = WebPushDevice
		fields = (
			"id", "name", "registration_id", "active", "date_created",
			"p256dh", "auth", "browser", "application_id"
		)


# Mixins
class DeviceViewSetMixin(object):
	lookup_field = "registration_id"

	def create(self, request, *args, **kwargs):
		serializer = None
		is_update = False
		data = request.data.copy()
		if SETTINGS.get("UPDATE_ON_DUPLICATE_REG_ID") and "registration_id" in request.data:
			instance = self.queryset.model.objects.filter(
				registration_id=request.data["registration_id"]
			).first()
			if instance:
				serializer = self.get_serializer(instance, data=request.data)
				is_update = True
		if not serializer:
			serializer = self.get_serializer(data=request.data)

		serializer.is_valid(raise_exception=True)
		if is_update:
			self.perform_update(serializer)
			return Response(serializer.data)
		else:
			self.perform_create(serializer)
			headers = self.get_success_headers(serializer.data)
			return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

	def perform_create(self, serializer):
		if self.request.user.is_authenticated:
			serializer.save(user=self.request.user)
		return super(DeviceViewSetMixin, self).perform_create(serializer)

	def perform_update(self, serializer):
		if self.request.user.is_authenticated:
			serializer.save(user=self.request.user)
		return super(DeviceViewSetMixin, self).perform_update(serializer)


# ViewSets
class APNSDeviceViewSet(DeviceViewSetMixin, ModelViewSet):

	queryset = APNSDevice.objects.all()
	serializer_class = APNSDeviceSerializer


class GCMDeviceViewSet(DeviceViewSetMixin, ModelViewSet):
	queryset = GCMDevice.objects.all()
	serializer_class = GCMDeviceSerializer


class WebPushDeviceViewSet(DeviceViewSetMixin, ModelViewSet):
	queryset = WebPushDevice.objects.all()
	serializer_class = WebPushDeviceSerializer
