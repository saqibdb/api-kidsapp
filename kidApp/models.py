from django.db import models


class User(models.Model):

    userName = models.CharField(blank=True, max_length=50)
    email = models.CharField(blank=True, max_length=50)
    password = models.CharField(blank=True, max_length=50)
    age = models.CharField(blank=True, max_length=50)
    phone = models.CharField(blank=True, max_length=50)
    gender = models.CharField(blank=True, max_length=20)
    isNotification = models.BooleanField(default=True)
    profilePictureURL = models.CharField(blank=True, max_length=200)
    deviceToken = models.CharField(blank=True, max_length=150)
    type = models.CharField(blank=True, max_length=10)
    studentIdentification = models.CharField(blank=True, max_length=100)
    creationDate =models.DateTimeField(auto_now_add=True)
    updationDate =models.DateTimeField(auto_now_add=True)
    lastLoginDate =models.DateTimeField(auto_now_add=True)
    authToken = models.CharField(blank=True, max_length=500)
    pin = models.CharField(blank=True, max_length=10)
    isPasswordPinChangeNeeded = models.BooleanField(default=True)
    isActive = models.BooleanField(default=True)
    branchId = models.IntegerField(default=0)
    fullName = models.CharField(blank=True, max_length=500)

    def __str__(self):
        return str(self.id)


class School(models.Model):
    schoolName = models.CharField(blank=True, max_length=50)
    schoolAddress = models.CharField(blank=True, max_length=100)
    creationDate =models.DateTimeField(auto_now_add=True)
    updationDate =models.DateTimeField(auto_now_add=True)
    isActive = models.BooleanField(default=True)

    def __str__(self):
        return str(self.id)

class Branch(models.Model):
    branchName = models.CharField(blank=True, max_length=50)
    branchAddress = models.CharField(blank=True, max_length=100)
    creationDate = models.DateTimeField(auto_now_add=True)
    updationDate =models.DateTimeField(auto_now_add=True)
    schoolId = models.IntegerField()
    isActive = models.BooleanField(default=True)

    def __str__(self):
        return str(self.id)


class Class(models.Model):
    className = models.CharField(blank=True, max_length=50)
    creationDate = models.DateTimeField(auto_now_add=True)
    updationDate =models.DateTimeField(auto_now_add=True)
    branchId = models.IntegerField()
    isActive = models.BooleanField(default=True)

    def __str__(self):
        return str(self.id)


class TeacherClass(models.Model):
    classId = models.IntegerField()
    teacherId = models.IntegerField()
    isClassTeacher = models.BooleanField(default=False)

    def __str__(self):
        return str(self.id)



class StudentParent(models.Model):
    studentId = models.IntegerField()
    parentId = models.IntegerField()

    def __str__(self):
        return str(self.id)



class StudentClass(models.Model):
    classId = models.IntegerField()
    studentId = models.IntegerField()

    def __str__(self):
        return str(self.id)


class SubjectClass(models.Model):
    classId = models.IntegerField()
    subjectId = models.IntegerField()

    def __str__(self):
        return str(self.id)

class SubjectPost(models.Model):
    postId = models.IntegerField()
    subjectId = models.IntegerField()

    def __str__(self):
        return str(self.id)


class Post(models.Model):
    postTitle = models.CharField(blank=True, max_length=1000)
    postDescription = models.CharField(blank=True, max_length=200)
    creationDate = models.DateTimeField(auto_now_add=True)
    updationDate =models.DateTimeField(auto_now_add=True)
    creatorId = models.IntegerField()
    classId = models.IntegerField()
    isActive = models.BooleanField(default=True)

    def __str__(self):
        return str(self.id)

class Comment(models.Model):
    commentTitle = models.CharField(blank=True, max_length=50)
    commentMessage = models.CharField(blank=True, max_length=200)
    creationDate = models.DateTimeField(auto_now_add=True)
    updationDate =models.DateTimeField(auto_now_add=True)
    postId = models.IntegerField()
    userId = models.IntegerField()
    isActive = models.BooleanField(default=True)

    def __str__(self):
        return str(self.id)

class Like(models.Model):
    creationDate = models.DateTimeField(auto_now_add=True)
    postId = models.IntegerField()
    userId = models.IntegerField()


    def __str__(self):
        return str(self.id)


class Media(models.Model):

    mediaLink = models.CharField(blank=True, max_length=1000)
    creationDate = models.DateTimeField(auto_now_add=True)
    postId = models.IntegerField()
    mediaType = models.CharField(blank=True, max_length=100)
    def __str__(self):
        return str(self.id)

class Tag(models.Model):

    tagText = models.CharField(blank=True, max_length=100)
    creationDate = models.DateTimeField(auto_now_add=True)
    userId = models.IntegerField()
    postId = models.IntegerField()

    def __str__(self):
        return str(self.id)


class Subject(models.Model):

    subjectName = models.CharField(blank=True, max_length=50)
    isActive = models.BooleanField(default=True)
    creationDate =models.DateTimeField(auto_now_add=True)
    updationDate =models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)
