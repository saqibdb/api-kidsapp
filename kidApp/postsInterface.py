import base64
from rest_framework.response import Response
from rest_framework.decorators import api_view
from kidApp.models import User, School, Branch, Class, TeacherClass, StudentParent, SubjectClass, SubjectPost ,Subject ,  Post, Comment, Like, Media, Tag
import uuid
import random
import json
from KidsApp.settings import MEDIA_URL, MEDIA_ROOT
from datetime import datetime

import boto
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import base64
import time

from push_notifications.models import APNSDevice, GCMDevice
from kidApp.push import fcm_send_notification, send_apns_notification



@api_view(['POST', ])
def AddPost(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            postTitle = body['post_title']
            postDescription = body['post_description']
            tagUsersIds = body['tag_users_ids']
            medias = body['medias']
            classId = body['class_id']

            users = User.objects.filter(authToken=str(auth)).values('id','type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            user = list(users)[0]
            if user['type'] == "Student":
                return Response({"Error" :"User of type "+user['type']+" do not have ability to add new Posts."})

            newPost = Post(postTitle=str(postTitle),
                    postDescription=str(postDescription),
                    creatorId=int(user['id']),
                    classId=int(classId))
            newPost.save()

            for media in medias:
                mediaLink = media['media_link']
                mediaType = media['media_type']
                newMedia= Media(mediaLink=str(mediaLink),
                        mediaType=str(mediaType),
                        postId=int(newPost.id))
                newMedia.save()

            posts = Post.objects.filter(id=int(newPost.id)).values('id',
                                                            'postTitle',
                                                            'postDescription',
                                                            'creatorId',
                                                            'creationDate',
                                                            'updationDate',
                                                            'classId')

            post = list(posts)[0]
            users = User.objects.filter(id=int(post['creatorId'])).values('id',
                                                            'userName',
                                                            'email',
                                                            'type',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'lastLoginDate',
                                                            'authToken')
            post['creator'] = list(users)[0]

            medias = Media.objects.filter(postId=int(post['id'])).values('id',
                                                            'mediaLink',
                                                            'mediaType',
                                                            'creationDate')
            post['medias'] = list(medias)

            comments = Comment.objects.filter(postId=int(post['id']),isActive=True).values('id',
                                                            'commentTitle',
                                                            'commentMessage',
                                                            'creationDate',
                                                            'updationDate',
                                                            'userId')
            post['total_comments'] = comments.count()
            allCommentsWithCreator = []
            for comment in comments:
                commentUsers = User.objects.filter(id=int(comment['userId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'type',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'deviceToken',
                                                                'lastLoginDate',
                                                                'authToken')
                comment['creator'] = list(commentUsers)[0]
                allCommentsWithCreator.append(comment)
            allCommentsWithCreator = sorted(allCommentsWithCreator, key=lambda x: x['updationDate'], reverse=True)
            post['comments'] = list(allCommentsWithCreator)
            likes = Like.objects.filter(postId=int(post['id']))
            post['total_likes'] = likes.count()
            for tagUsersIdObj in tagUsersIds:
                newTag = Tag(tagText=str(""),
                    userId=str(tagUsersIdObj['id']),
                    postId=str(post['id']))
                newTag.save()


            tags = Tag.objects.filter(postId=int(post['id'])).values('id',
                                                            'userId',
                                                            'postId')
            allTaggedUsers = []
            for tag in tags:
                taggedUsers = User.objects.filter(id=int(tag['userId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'type',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'lastLoginDate')
                if taggedUsers.count() > 0:
                    allTaggedUsers.append(list(taggedUsers)[0])
            post['tagged_users'] = list(allTaggedUsers)

            likes = Like.objects.filter(postId=int(post['id'])).values('userId')
            likingUsers = []
            for like in likes:
                likedUsers = User.objects.filter(id=int(like['userId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'type',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'lastLoginDate')

                if likedUsers.count() > 0:
                    likingUsers.append(list(likedUsers)[0])

            post['total_likes'] = likes.count()
            post['liking_users'] = list(likingUsers)
            return Response({"NewPost" :post})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})




@api_view(['POST', ])
def GetMyCreatedPosts(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            page = int(body['page'])
            itemsPerPage = int(body['per_page'])
            users = User.objects.filter(authToken=str(auth)).values('id',
                                                            'userName',
                                                            'email',
                                                            'type',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'lastLoginDate',
                                                            'authToken')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            user = list(users)[0]
            posts = Post.objects.filter(creatorId=int(user['id']),isActive=True).values('id',
                                                            'postTitle',
                                                            'postDescription',
                                                            'creatorId',
                                                            'creationDate',
                                                            'updationDate',
                                                            'classId')
            allPostsWithCreatorAndMedia = []
            for post in posts:
                post['creator'] = user
                medias = Media.objects.filter(postId=int(post['id'])).values('id',
                                                                'mediaLink',
                                                                'mediaType',
                                                                'creationDate')
                post['medias'] = list(medias)
                comments = Comment.objects.filter(postId=int(post['id']),isActive=True)
                post['total_comments'] = comments.count()
                allCommentsWithCreator = []
                for comment in comments:
                    commentUsers = User.objects.filter(id=int(comment['userId'])).values('id',
                                                                    'userName',
                                                                    'email',
                                                                    'type',
                                                                    'age',
                                                                    'phone',
                                                                    'gender',
                                                                    'profilePictureURL',
                                                                    'lastLoginDate')
                    comment['creator'] = list(commentUsers)[0]
                    allCommentsWithCreator.append(comment)
                allCommentsWithCreator = sorted(allCommentsWithCreator, key=lambda x: x['updationDate'], reverse=True)
                post['comments'] = list(allCommentsWithCreator)
                likes = Like.objects.filter(postId=int(post['id'])).values('userId')
                likingUsers = []
                for like in likes:
                    likedUsers = User.objects.filter(id=int(like['userId'])).values('id',
                                                                    'userName',
                                                                    'email',
                                                                    'type',
                                                                    'age',
                                                                    'phone',
                                                                    'gender',
                                                                    'profilePictureURL',
                                                                    'lastLoginDate')

                    if likedUsers.count() > 0:
                        likingUsers.append(list(likedUsers)[0])

                post['total_likes'] = likes.count()
                post['liking_users'] = list(likingUsers)

                tags = Tag.objects.filter(postId=int(post['id'])).values('id',
                                                                'userId',
                                                                'postId')
                allTaggedUsers = []
                for tag in tags:
                    taggedUsers = User.objects.filter(id=int(tag['userId'])).values('id',
                                                                    'userName',
                                                                    'email',
                                                                    'type',
                                                                    'age',
                                                                    'phone',
                                                                    'gender',
                                                                    'profilePictureURL',
                                                                    'lastLoginDate')
                    if taggedUsers.count() > 0:
                        allTaggedUsers.append(list(taggedUsers)[0])
                post['tagged_users'] = list(allTaggedUsers)

                allPostsWithCreatorAndMedia.append(post)
            left = len(allPostsWithCreatorAndMedia) - (page*itemsPerPage)
            next_page = True
            if left > 0:
                next_page = True
            else:
                next_page = False
            if page == -1:
                return Response({"Posts": allPostsWithCreatorAndMedia, "Total" : len(allPostsWithCreatorAndMedia), "NextPage" : next_page})
            else:
                return Response({"Posts": allPostsWithCreatorAndMedia[(page*itemsPerPage)-itemsPerPage:page*itemsPerPage], "Total" : len(allPostsWithCreatorAndMedia), "NextPage" : next_page})

        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def GetTaggedPosts(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            page = int(body['page'])
            itemsPerPage = int(body['per_page'])
            users = User.objects.filter(authToken=str(auth)).values('id',
                                                            'userName',
                                                            'email',
                                                            'type',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'lastLoginDate',
                                                            'authToken')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            user = list(users)[0]

            tags = Tag.objects.filter(userId=int(user['id'])).values('id',
                                                            'postId')
            allPostsWithCreatorAndMedia = []
            for tag in tags:
                posts = Post.objects.filter(id=int(tag['postId']),isActive=True).values('id',
                                                            'postTitle',
                                                            'postDescription',
                                                            'creatorId',
                                                            'creationDate',
                                                            'updationDate',
                                                            'classId')
                for post in posts:
                    users = User.objects.filter(id=int(post['creatorId'])).values('id',
                                                                    'userName',
                                                                    'email',
                                                                    'type',
                                                                    'age',
                                                                    'phone',
                                                                    'gender',
                                                                    'profilePictureURL',
                                                                    'deviceToken',
                                                                    'lastLoginDate',
                                                                    'authToken')
                    post['creator'] = list(users)[0]
                    medias = Media.objects.filter(postId=int(post['id'])).values('id',
                                                                    'mediaLink',
                                                                    'mediaType',
                                                                    'creationDate')
                    post['medias'] = list(medias)
                    comments = Comment.objects.filter(postId=int(post['id']),isActive=True)
                    post['total_comments'] = comments.count()
                    allCommentsWithCreator = []
                    for comment in comments:
                        commentUsers = User.objects.filter(id=int(comment['userId'])).values('id',
                                                                        'userName',
                                                                        'email',
                                                                        'type',
                                                                        'age',
                                                                        'phone',
                                                                        'gender',
                                                                        'profilePictureURL',
                                                                        'lastLoginDate')
                        comment['creator'] = list(commentUsers)[0]
                        allCommentsWithCreator.append(comment)
                    allCommentsWithCreator = sorted(allCommentsWithCreator, key=lambda x: x['updationDate'], reverse=True)
                    post['comments'] = list(allCommentsWithCreator)
                    likes = Like.objects.filter(postId=int(post['id'])).values('userId')
                    likingUsers = []
                    for like in likes:
                        likedUsers = User.objects.filter(id=int(like['userId'])).values('id',
                                                                        'userName',
                                                                        'email',
                                                                        'type',
                                                                        'age',
                                                                        'phone',
                                                                        'gender',
                                                                        'profilePictureURL',
                                                                        'lastLoginDate')

                        if likedUsers.count() > 0:
                            likingUsers.append(list(likedUsers)[0])

                    post['total_likes'] = likes.count()
                    post['liking_users'] = list(likingUsers)

                    tags = Tag.objects.filter(postId=int(post['id'])).values('id',
                                                                    'userId',
                                                                    'postId')
                    allTaggedUsers = []
                    for tag in tags:
                        taggedUsers = User.objects.filter(id=int(tag['userId'])).values('id',
                                                                        'userName',
                                                                        'email',
                                                                        'type',
                                                                        'age',
                                                                        'phone',
                                                                        'gender',
                                                                        'profilePictureURL',
                                                                        'lastLoginDate')
                        if taggedUsers.count() > 0:
                            allTaggedUsers.append(list(taggedUsers)[0])
                    post['tagged_users'] = list(allTaggedUsers)



                    allPostsWithCreatorAndMedia.append(post)
            left = len(allPostsWithCreatorAndMedia) - (page*itemsPerPage)
            next_page = True
            if left > 0:
                next_page = True
            else:
                next_page = False
            if page == -1:
                return Response({"Posts": allPostsWithCreatorAndMedia, "Total" : len(allPostsWithCreatorAndMedia), "NextPage" : next_page})
            else:
                return Response({"Posts": allPostsWithCreatorAndMedia[(page*itemsPerPage)-itemsPerPage:page*itemsPerPage], "Total" : len(allPostsWithCreatorAndMedia), "NextPage" : next_page})

        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})




@api_view(['POST', ])
def AddCommentToPost(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            commentTitle = body['comment_title']
            commentMessage = ""
            postId = body['post_id']


            users = User.objects.filter(authToken=str(auth)).values('id','type','userName')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            user = list(users)[0]
            if user['type'] == "Student":
                return Response({"Error" :"User of type "+user['type']+" do not have ability to add new Posts."})

            newComment = Comment(commentTitle=str(commentTitle),
                    commentMessage=str(commentMessage),
                    postId=int(postId),
                    userId=int(user['id']))
            newComment.save()

            posts = Post.objects.filter(id=int(postId)).values('id',
                                                            'postTitle',
                                                            'postDescription',
                                                            'creatorId',
                                                            'creationDate',
                                                            'updationDate',
                                                            'classId')

            post = list(posts)[0]
            users = User.objects.filter(id=int(post['creatorId'])).values('id',
                                                            'userName',
                                                            'email',
                                                            'type',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'profilePictureURL')
            post['creator'] = list(users)[0]
            medias = Media.objects.filter(postId=int(post['id'])).values('id',
                                                            'mediaLink',
                                                            'mediaType',
                                                            'creationDate')
            post['medias'] = list(medias)
            comments = Comment.objects.filter(postId=int(post['id']),isActive=True).values('id',
                                                            'commentTitle',
                                                            'commentMessage',
                                                            'creationDate',
                                                            'updationDate',
                                                            'userId')
            post['total_comments'] = comments.count()
            allCommentsWithCreator = []
            for comment in comments:
                commentUsers = User.objects.filter(id=int(comment['userId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'type',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'lastLoginDate')
                comment['creator'] = list(commentUsers)[0]
                allCommentsWithCreator.append(comment)
            allCommentsWithCreator = sorted(allCommentsWithCreator, key=lambda x: x['updationDate'], reverse=True)
            post['comments'] = list(allCommentsWithCreator)
            likes = Like.objects.filter(postId=int(post['id'])).values('userId')
            likingUsers = []
            for like in likes:
                likedUsers = User.objects.filter(id=int(like['userId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'type',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'lastLoginDate')
                if likedUsers.count() > 0:
                    likingUsers.append(list(likedUsers)[0])
            post['total_likes'] = likes.count()
            post['liking_users'] = list(likingUsers)

            tags = Tag.objects.filter(postId=int(post['id'])).values('id',
                                                            'userId',
                                                            'postId')
            allTaggedUsers = []
            for tag in tags:
                taggedUsers = User.objects.filter(id=int(tag['userId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'type',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'lastLoginDate')
                if taggedUsers.count() > 0:
                    taggedUser = list(taggedUsers)[0]
                    if taggedUser['type'] == "Student":
                        studentParents = StudentParent.objects.filter(studentId=int(taggedUser['id'])).values('id',
                                                                                                              'studentId',
                                                                                                              'parentId')
                        for studentParent in studentParents:
                            parents = User.objects.filter(id=int(studentParent['parentId'])).values('id', 'userName', 'email', 'type', 'age', 'phone', 'gender', 'profilePictureURL', 'lastLoginDate')
                            allTaggedUsers.extend(list(parents))
                    else:
                        allTaggedUsers.append(list(taggedUsers)[0])
            post['tagged_users'] = list(allTaggedUsers)

            pushTitle = user['userName'] + " has commented " + commentTitle

            pushIds = []
            if post['creator']['type'] != "SuperAdmin":
                if user['id'] != post['creator']['id']:
                    pushIds.append(int(post['creator']['id']))

            for taggedUser in post['tagged_users']:
                if user['id'] != taggedUser['id']:
                    if int(taggedUser['id']) not in pushIds:
                        pushIds.append(int(taggedUser['id']))

            if len(pushIds) > 0:
                send_apns_notification(pushIds, int(post['id']), str(pushTitle))
                statusFCM = fcm_send_notification(pushIds, int(post['id']), title=str(pushTitle), body="")
            post['pushIds'] = pushIds
            return Response({"NewPost" :post})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})




@api_view(['POST', ])
def GetAllTimeLinePosts(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            page = int(body['page'])
            itemsPerPage = int(body['per_page'])
            users = User.objects.filter(authToken=str(auth)).values('id',
                                                            'userName',
                                                            'email',
                                                            'type',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'lastLoginDate',
                                                            'authToken')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            user = list(users)[0]




            tags = Tag.objects.filter(userId=int(user['id'])).values('id',
                                                            'postId')
            allPostsWithCreatorAndMedia = []
            allPostsCombinedWithoutCreatorAndMedia = []


            createdPosts = Post.objects.filter(creatorId=int(user['id']),isActive=True).values('id',
                                                            'postTitle',
                                                            'postDescription',
                                                            'creatorId',
                                                            'creationDate',
                                                            'updationDate',
                                                            'classId')

            allPostsCombinedWithoutCreatorAndMedia.extend(createdPosts)

            for tag in tags:
                posts = Post.objects.filter(id=int(tag['postId']),isActive=True).values('id',
                                                            'postTitle',
                                                            'postDescription',
                                                            'creatorId',
                                                            'creationDate',
                                                            'updationDate',
                                                            'classId')
                allPostsCombinedWithoutCreatorAndMedia.extend(posts)





            if user['type'] == "Teacher":
                teacherClasses = TeacherClass.objects.filter(teacherId=int(user['id'])).values('id',
                                                'classId',
                                                'teacherId',
                                                'isClassTeacher')
                if teacherClasses.count() > 0:
                    for teacherClass in teacherClasses:
                        classes = Class.objects.filter(id=int(teacherClass['classId'])).values('id')
                        for classe in classes:
                            classe['isClassTeacher'] = teacherClass['isClassTeacher']
                        classe = list(classes)[0]
                        postsWithClassId = Post.objects.filter(classId=int(classe['id'])).values('id',
                                                                    'postTitle',
                                                                    'postDescription',
                                                                    'creatorId',
                                                                    'creationDate',
                                                                    'updationDate',
                                                                    'classId')
                        for post in postsWithClassId:
                            if post not in allPostsCombinedWithoutCreatorAndMedia:
                                allPostsCombinedWithoutCreatorAndMedia.append(post)



            for post in allPostsCombinedWithoutCreatorAndMedia:
                users = User.objects.filter(id=int(post['creatorId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'type',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'profilePictureURL')
                post['creator'] = list(users)[0]
                medias = Media.objects.filter(postId=int(post['id'])).values('id',
                                                                'mediaLink',
                                                                'mediaType',
                                                                'creationDate')
                post['medias'] = list(medias)
                comments = Comment.objects.filter(postId=int(post['id']),isActive=True)
                post['total_comments'] = comments.count()

                '''
                for comment in comments:
                    commentUsers = User.objects.filter(id=int(comment['userId'])).values('id',
                                                                    'userName',
                                                                    'email',
                                                                    'type',
                                                                    'age',
                                                                    'phone',
                                                                    'gender',
                                                                    'profilePictureURL',
                                                                    'lastLoginDate')
                    comment['creator'] = list(commentUsers)[0]
                    allCommentsWithCreator.append(comment)
                allCommentsWithCreator = sorted(allCommentsWithCreator, key=lambda x: x['updationDate'], reverse=True)
                post['comments'] = list(allCommentsWithCreator)
                '''


                likes = Like.objects.filter(postId=int(post['id'])).values('userId')
                likingUsers = []
                for like in likes:
                    likedUsers = User.objects.filter(id=int(like['userId'])).values('id',
                                                                    'userName',
                                                                    'email',
                                                                    'type',
                                                                    'age',
                                                                    'phone',
                                                                    'gender',
                                                                    'profilePictureURL',
                                                                    'lastLoginDate')
                    if likedUsers.count() > 0:
                        likingUsers.append(list(likedUsers)[0])
                post['total_likes'] = likes.count()
                post['liking_users'] = list(likingUsers)

                tags = Tag.objects.filter(postId=int(post['id'])).values('id',
                                                                'userId',
                                                                'postId')
                allTaggedUsers = []
                for tag in tags:
                    taggedUsers = User.objects.filter(id=int(tag['userId'])).values('id',
                                                                    'userName',
                                                                    'email',
                                                                    'type',
                                                                    'age',
                                                                    'phone',
                                                                    'gender',
                                                                    'profilePictureURL',
                                                                    'lastLoginDate')
                    if taggedUsers.count() > 0:
                        allTaggedUsers.append(list(taggedUsers)[0])
                post['tagged_users'] = list(allTaggedUsers)
                allPostsWithCreatorAndMedia.append(post)



            allPostsWithCreatorAndMedia = sorted(allPostsWithCreatorAndMedia, key=lambda x: x['updationDate'], reverse=True)
            left = len(allPostsWithCreatorAndMedia) - (page*itemsPerPage)
            next_page = True
            if left > 0:
                next_page = True
            else:
                next_page = False
            if page == -1:
                return Response({"Posts": allPostsWithCreatorAndMedia, "Total" : len(allPostsWithCreatorAndMedia), "NextPage" : next_page})
            else:
                return Response({"Posts": allPostsWithCreatorAndMedia[(page*itemsPerPage)-itemsPerPage:page*itemsPerPage], "Total" : len(allPostsWithCreatorAndMedia), "NextPage" : next_page})

        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})





@api_view(['POST', ])
def GetTimeLinePostsForDate(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            page = int(body['page'])
            itemsPerPage = int(body['per_page'])
            s = str(body['date'])
            f = "%Y-%m-%d"
            date = datetime.strptime(s, f)
            users = User.objects.filter(authToken=str(auth)).values('id',
                                                            'userName',
                                                            'email',
                                                            'type',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'lastLoginDate',
                                                            'authToken')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            user = list(users)[0]

            tags = Tag.objects.filter(userId=int(user['id'])).values('id',
                                                            'postId')
            allPostsWithCreatorAndMedia = []
            allPostsCombinedWithoutCreatorAndMedia = []
            for tag in tags:
                posts = Post.objects.filter(id=int(tag['postId']),isActive=True).values('id',
                                                            'postTitle',
                                                            'postDescription',
                                                            'creatorId',
                                                            'creationDate',
                                                            'updationDate',
                                                            'classId')
                allPostsCombinedWithoutCreatorAndMedia.extend(posts)
            if user['type'] == "Teacher":
                teacherClasses = TeacherClass.objects.filter(teacherId=int(user['id'])).values('id',
                                                'classId',
                                                'teacherId',
                                                'isClassTeacher')
                if teacherClasses.count() > 0:
                    for teacherClass in teacherClasses:
                        classes = Class.objects.filter(id=int(teacherClass['classId'])).values('id')
                        for classe in classes:
                            classe['isClassTeacher'] = teacherClass['isClassTeacher']
                        classe = list(classes)[0]
                        postsWithClassId = Post.objects.filter(classId=int(classe['id']),isActive=True).values('id',
                                                                    'postTitle',
                                                                    'postDescription',
                                                                    'creatorId',
                                                                    'creationDate',
                                                                    'updationDate',
                                                                    'classId')


                        for post in postsWithClassId:
                            if post not in allPostsCombinedWithoutCreatorAndMedia:
                                allPostsCombinedWithoutCreatorAndMedia.append(post)
            for post in allPostsCombinedWithoutCreatorAndMedia:
                users = User.objects.filter(id=int(post['creatorId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'type',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'deviceToken',
                                                                'lastLoginDate',
                                                                'authToken')
                post['creator'] = list(users)[0]
                medias = Media.objects.filter(postId=int(post['id'])).values('id',
                                                                'mediaLink',
                                                                'mediaType',
                                                                'creationDate')
                post['medias'] = list(medias)
                comments = Comment.objects.filter(postId=int(post['id']),isActive=True)
                post['total_comments'] = comments.count()
                allCommentsWithCreator = []
                for comment in comments:
                    commentUsers = User.objects.filter(id=int(comment['userId'])).values('id',
                                                                    'userName',
                                                                    'email',
                                                                    'type',
                                                                    'age',
                                                                    'phone',
                                                                    'gender',
                                                                    'profilePictureURL',
                                                                    'lastLoginDate')
                    comment['creator'] = list(commentUsers)[0]
                    allCommentsWithCreator.append(comment)
                allCommentsWithCreator = sorted(allCommentsWithCreator, key=lambda x: x['updationDate'], reverse=True)
                post['comments'] = list(allCommentsWithCreator)
                likes = Like.objects.filter(postId=int(post['id'])).values('userId')
                likingUsers = []
                for like in likes:
                    likedUsers = User.objects.filter(id=int(like['userId'])).values('id',
                                                                    'userName',
                                                                    'email',
                                                                    'type',
                                                                    'age',
                                                                    'phone',
                                                                    'gender',
                                                                    'profilePictureURL',
                                                                    'lastLoginDate')
                    if likedUsers.count() > 0:
                        likingUsers.append(list(likedUsers)[0])
                post['total_likes'] = likes.count()
                post['liking_users'] = list(likingUsers)

                tags = Tag.objects.filter(postId=int(post['id'])).values('id',
                                                                'userId',
                                                                'postId')
                allTaggedUsers = []
                for tag in tags:
                    taggedUsers = User.objects.filter(id=int(tag['userId'])).values('id',
                                                                    'userName',
                                                                    'email',
                                                                    'type',
                                                                    'age',
                                                                    'phone',
                                                                    'gender',
                                                                    'profilePictureURL',
                                                                    'lastLoginDate')
                    if taggedUsers.count() > 0:
                        allTaggedUsers.append(list(taggedUsers)[0])
                post['tagged_users'] = list(allTaggedUsers)
                allPostsWithCreatorAndMedia.append(post)
            allPostsWithCreatorAndMediaAndSpecifiedDate = []
            for post in allPostsWithCreatorAndMedia:
                if post['creationDate'].date() == date.date():
                    allPostsWithCreatorAndMediaAndSpecifiedDate.append(post)
            allPostsWithCreatorAndMediaAndSpecifiedDate = sorted(allPostsWithCreatorAndMediaAndSpecifiedDate, key=lambda x: x['updationDate'])
            left = len(allPostsWithCreatorAndMediaAndSpecifiedDate) - (page*itemsPerPage)
            next_page = True
            if left > 0:
                next_page = True
            else:
                next_page = False
            if page == -1:
                return Response({"Posts": allPostsWithCreatorAndMediaAndSpecifiedDate, "Total" : len(allPostsWithCreatorAndMediaAndSpecifiedDate), "NextPage" : next_page})
            else:
                return Response({"Posts": allPostsWithCreatorAndMediaAndSpecifiedDate[(page*itemsPerPage)-itemsPerPage:page*itemsPerPage], "Total" : len(allPostsWithCreatorAndMediaAndSpecifiedDate), "NextPage" : next_page})

        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def GetAllTimeLinePostsWithFilters(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            page = int(body['page'])
            itemsPerPage = int(body['per_page'])
            filters = body['filters']
            users = User.objects.filter(authToken=str(auth)).values('id',
                                                            'userName',
                                                            'email',
                                                            'type',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'lastLoginDate',
                                                            'authToken',
                                                            'branchId')

            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            user = list(users)[0]
            tags = []
            userTags = Tag.objects.filter(userId=int(user['id'])).values('id',
                                                            'postId')
            tags.extend(userTags)
            if user['type'] == "Parent":
                studentParents = StudentParent.objects.filter(parentId=int(user['id'])).values('id',
                                                                'studentId',
                                                                'parentId')
                allChilds = []
                for studentParent in studentParents:
                    childs = User.objects.filter(id=int(studentParent['studentId'])).values('id')
                    allChilds.extend(childs)
                for child in allChilds:
                    childTags = Tag.objects.filter(userId=int(child['id'])).values('id',
                                                                    'postId')
                    tags.extend(childTags)

            allPostsWithCreatorAndMedia = []
            allPostsCombinedWithoutCreatorAndMedia = []
            createdPosts = Post.objects.filter(creatorId=int(user['id']),isActive=True).values('id',
                                                            'postTitle',
                                                            'postDescription',
                                                            'creatorId',
                                                            'creationDate',
                                                            'updationDate',
                                                            'classId')
            allPostsCombinedWithoutCreatorAndMedia.extend(createdPosts)
            for tag in tags:
                posts = Post.objects.filter(id=int(tag['postId']),isActive=True).values('id',
                                                            'postTitle',
                                                            'postDescription',
                                                            'creatorId',
                                                            'creationDate',
                                                            'updationDate',
                                                            'classId')
                allPostsCombinedWithoutCreatorAndMedia.extend(posts)
            if user['type'] == "Teacher":
                teacherClasses = TeacherClass.objects.filter(teacherId=int(user['id'])).values('id',
                                                'classId',
                                                'teacherId',
                                                'isClassTeacher')
                if teacherClasses.count() > 0:
                    for teacherClass in teacherClasses:
                        classes = Class.objects.filter(id=int(teacherClass['classId'])).values('id')
                        for classe in classes:
                            classe['isClassTeacher'] = teacherClass['isClassTeacher']
                        classe = list(classes)[0]
                        postsWithClassId = Post.objects.filter(classId=int(classe['id']),isActive=True).values('id',
                                                                    'postTitle',
                                                                    'postDescription',
                                                                    'creatorId',
                                                                    'creationDate',
                                                                    'updationDate',
                                                                    'classId')
                        for post in postsWithClassId:
                            if post not in allPostsCombinedWithoutCreatorAndMedia:
                                creators = User.objects.filter(id=int(post['creatorId'])).values('id', 'type')
                                creator = list(creators)[0]
                                if creator['type'] == "Teacher":
                                    if post['creatorId'] == user['id']:
                                        allPostsCombinedWithoutCreatorAndMedia.append(post)
                                else:
                                    allPostsCombinedWithoutCreatorAndMedia.append(post)



            if user['type'] == "SuperAdmin":
                postsWithClassId = Post.objects.filter(isActive=True).values('id',
                                                            'postTitle',
                                                            'postDescription',
                                                            'creatorId',
                                                            'creationDate',
                                                            'updationDate',
                                                            'classId')
                for post in postsWithClassId:
                    if post not in allPostsCombinedWithoutCreatorAndMedia:
                        allPostsCombinedWithoutCreatorAndMedia.append(post)

            if user['type'] == "S-Admin":
                allClasses = []
                allBranches = Branch.objects.filter(schoolId=int(user['branchId']), isActive=True).values('id')
                for branch in allBranches:
                    classes = Class.objects.filter(branchId=int(branch['id']), isActive=True).values('id')
                    allClasses.extend(classes)
                    for classe in allClasses:
                        postsWithClassId = Post.objects.filter(classId=int(classe['id']),isActive=True).values('id',
                                                                                     'postTitle',
                                                                                     'postDescription',
                                                                                     'creatorId',
                                                                                     'creationDate',
                                                                                     'updationDate',
                                                                                     'classId')
                        for post in postsWithClassId:
                            if post not in allPostsCombinedWithoutCreatorAndMedia:
                                allPostsCombinedWithoutCreatorAndMedia.append(post)
            if user['type'] == "B-Admin":
                allClasses = []
                classes = Class.objects.filter(branchId=int(user['branchId']), isActive=True).values('id',
                                                                                                     'className',
                                                                                                     'creationDate',
                                                                                                     'updationDate',
                                                                                                     'branchId')
                allClasses.extend(classes)
                for classe in allClasses:
                    postsWithClassId = Post.objects.filter(classId=int(classe['id']), isActive=True).values('id',
                                                                                                            'postTitle',
                                                                                                            'postDescription',
                                                                                                            'creatorId',
                                                                                                            'creationDate',
                                                                                                            'updationDate',
                                                                                                            'classId')
                    for post in postsWithClassId:
                        if post not in allPostsCombinedWithoutCreatorAndMedia:
                            allPostsCombinedWithoutCreatorAndMedia.append(post)



#TODO NEED TO HANDLE OTHER USERS



            for post in allPostsCombinedWithoutCreatorAndMedia:


                users = User.objects.filter(id=int(post['creatorId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'type',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'profilePictureURL')
                post['creator'] = list(users)[0]

                medias = Media.objects.filter(postId=int(post['id'])).values('id',
                                                                'mediaLink',
                                                                'mediaType',
                                                                'creationDate')

                post['medias'] = list(medias)



                comments = Comment.objects.filter(postId=int(post['id']),isActive=True)
                post['total_comments'] = comments.count()



                likes = Like.objects.filter(postId=int(post['id'])).values('userId')
                likingUsers = []
                for like in likes:
                    likedUsers = User.objects.filter(id=int(like['userId'])).values('id',
                                                                    'userName',
                                                                    'email',
                                                                    'type',
                                                                    'age',
                                                                    'phone',
                                                                    'gender',
                                                                    'profilePictureURL',
                                                                    'lastLoginDate')
                    if likedUsers.count() > 0:
                        likingUsers.append(list(likedUsers)[0])
                post['total_likes'] = likes.count()
                post['liking_users'] = list(likingUsers)

                allSubjects = []
                subjectPosts = SubjectPost.objects.filter(postId=int(post['id'])).values('id',
                                                                'subjectId')
                for subjectPost in subjectPosts:
                    subjects = Subject.objects.filter(id=int(subjectPost['subjectId'])).values('id',
                                                                    'subjectName')
                    allSubjects.extend(subjects)
                post['subjects'] = allSubjects






                tags = Tag.objects.filter(postId=int(post['id'])).values('id',
                                                                'userId',
                                                                'postId')
                allTaggedUsers = []

                for tag in tags:
                    taggedUsers = User.objects.filter(id=int(tag['userId'])).values('id',
                                                                    'userName',
                                                                    'email',
                                                                    'type',
                                                                    'age',
                                                                    'phone',
                                                                    'gender',
                                                                    'profilePictureURL',
                                                                    'lastLoginDate')
                    if taggedUsers.count() > 0:
                        allTaggedUsers.append(list(taggedUsers)[0])

                post['tagged_users'] = list(allTaggedUsers)

                if post not in allPostsWithCreatorAndMedia:
                    allPostsWithCreatorAndMedia.append(post)


            allPostsWithCreatorAndMedia = sorted(allPostsWithCreatorAndMedia, key=lambda x: x['updationDate'], reverse=True)

            allPostsWithCreatorAndMediaFiltered = []

            allPostsWithCreatorAndMediaFiltered = allPostsWithCreatorAndMedia
            for filterObj in filters:

                if filterObj['filter_name'] == "date":
                    s = str(filterObj['filter_value'])
                    f = "%Y-%m-%d"
                    date = datetime.strptime(s, f)

                    for post in list(allPostsWithCreatorAndMediaFiltered):
                        if post['creationDate'].date() != date.date():
                            allPostsWithCreatorAndMediaFiltered.remove(post)



                if filterObj['filter_name'] == "class":
                    classId = int(filterObj['filter_value'])
                    for post in list(allPostsWithCreatorAndMediaFiltered):
                        if post['classId'] != classId:
                            allPostsWithCreatorAndMediaFiltered.remove(post)



            left = len(allPostsWithCreatorAndMediaFiltered) - (page*itemsPerPage)
            next_page = True
            if left > 0:
                next_page = True
            else:
                next_page = False
            if page == -1:
                return Response({"Posts": allPostsWithCreatorAndMediaFiltered, "Total" : len(allPostsWithCreatorAndMediaFiltered), "NextPage" : next_page})
            else:
                return Response({"Posts": allPostsWithCreatorAndMediaFiltered[(page*itemsPerPage)-itemsPerPage:page*itemsPerPage], "Total" : len(allPostsWithCreatorAndMediaFiltered), "NextPage" : next_page})

        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})









@api_view(['POST', ])
def GetDetailsOfPost(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            postID = int(body['post_id'])
            users = User.objects.filter(authToken=str(auth)).values('id',
                                                            'userName',
                                                            'email',
                                                            'type',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'lastLoginDate',
                                                            'authToken')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            user = list(users)[0]


            posts = Post.objects.filter(id=int(postID),isActive=True).values('id',
                                                        'postTitle',
                                                        'postDescription',
                                                        'creatorId',
                                                        'creationDate',
                                                        'updationDate',
                                                        'classId')
            if posts.count() == 0:
                return Response({"Error" :"No Post Found with specified Id."})
            post = list(posts)[0]
            users = User.objects.filter(id=int(post['creatorId'])).values('id',
                                                            'userName',
                                                            'email',
                                                            'type',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'profilePictureURL')
            post['creator'] = list(users)[0]
            medias = Media.objects.filter(postId=int(post['id'])).values('id',
                                                            'mediaLink',
                                                            'mediaType',
                                                            'creationDate')
            post['medias'] = list(medias)
            comments = Comment.objects.filter(postId=int(post['id']),isActive=True).values('id',
                                                            'commentTitle',
                                                            'commentMessage',
                                                            'creationDate',
                                                            'updationDate',
                                                            'userId')
            post['total_comments'] = comments.count()
            allCommentsWithCreator = []
            for comment in comments:
                commentUsers = User.objects.filter(id=int(comment['userId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'type',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'lastLoginDate')
                comment['creator'] = list(commentUsers)[0]
                allCommentsWithCreator.append(comment)
            allCommentsWithCreator = sorted(allCommentsWithCreator, key=lambda x: x['updationDate'], reverse=True)
            post['comments'] = list(allCommentsWithCreator)
            likes = Like.objects.filter(postId=int(post['id'])).values('userId')
            likingUsers = []
            for like in likes:
                likedUsers = User.objects.filter(id=int(like['userId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'type',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'lastLoginDate')
                if likedUsers.count() > 0:
                    likingUsers.append(list(likedUsers)[0])
            post['total_likes'] = likes.count()
            post['liking_users'] = list(likingUsers)
            tags = Tag.objects.filter(postId=int(post['id'])).values('id',
                                                            'userId',
                                                            'postId')
            allTaggedUsers = []
            for tag in tags:
                taggedUsers = User.objects.filter(id=int(tag['userId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'type',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'lastLoginDate')
                if taggedUsers.count() > 0:
                    allTaggedUsers.append(list(taggedUsers)[0])
            post['tagged_users'] = list(allTaggedUsers)
            return Response({"Post": post})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})






@api_view(['POST', ])
def AddPostWithMedia(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            postTitle = body['post_title']
            postDescription = body['post_description']
            tagUsersIds = body['tag_users_ids']
            medias = body['medias']
            classId = body['class_id']

            users = User.objects.filter(authToken=str(auth)).values('id','type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            user = list(users)[0]
            if user['type'] == "Student":
                return Response({"Error" :"User of type "+user['type']+" do not have ability to add new Posts."})

            for media in medias:
                mediaData = media['media_data']
                extension = ".jpg"
                contentType = "image/jpeg"
                if media['media_type'] == "video":
                    extension = ".mp4"
                    contentType = "video/mp4"
                AWS_ACCESS_KEY = 'AKIAJBOEQASD4L7YUKFQ'
                AWS_SECRET_KEY = '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'
                AWS_BUCKET_NAME = 'uchoose'
                connection = boto.s3.connect_to_region('us-east-1',
                                                        aws_access_key_id=AWS_ACCESS_KEY,
                                                        aws_secret_access_key=AWS_SECRET_KEY,
                                                        #is_secure=True,               # uncomment if you are not using ssl
                                                        calling_format = boto.s3.connection.OrdinaryCallingFormat(),
                                                        )
                bucket = connection.get_bucket(AWS_BUCKET_NAME)
                cloudfile = Key(bucket)
                millis = int(round(time.time() * 1000))
                path = "KidsApp/" + str(user['id']) + str(millis) + ".jpg"
                cloudfile.key = path
                cloudfile.set_contents_from_string(base64.b64decode(mediaData))
                cloudfile.set_metadata('Content-Type', contentType)
                cloudfile.set_acl('public-read')
                media['media_link'] = "https://s3.amazonaws.com/uchoose/" + path

            newPost = Post(postTitle=str(postTitle),
                    postDescription=str(postDescription),
                    creatorId=int(user['id']),
                    classId=int(classId))
            newPost.save()
            for media in medias:
                mediaLink = media['media_link']
                mediaType = media['media_type']
                newMedia= Media(mediaLink=str(mediaLink),
                        mediaType=str(mediaType),
                        postId=int(newPost.id))
                newMedia.save()
            posts = Post.objects.filter(id=int(newPost.id)).values('id',
                                                            'postTitle',
                                                            'postDescription',
                                                            'creatorId',
                                                            'creationDate',
                                                            'updationDate',
                                                            'classId')
            post = list(posts)[0]
            users = User.objects.filter(id=int(post['creatorId'])).values('id',
                                                            'userName',
                                                            'email',
                                                            'type',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'lastLoginDate',
                                                            'authToken')
            post['creator'] = list(users)[0]

            medias = Media.objects.filter(postId=int(post['id'])).values('id',
                                                            'mediaLink',
                                                            'mediaType',
                                                            'creationDate')
            post['medias'] = list(medias)
            comments = Comment.objects.filter(postId=int(post['id']),isActive=True).values('id',
                                                            'commentTitle',
                                                            'commentMessage',
                                                            'creationDate',
                                                            'updationDate',
                                                            'userId')
            post['total_comments'] = comments.count()
            allCommentsWithCreator = []
            for comment in comments:
                commentUsers = User.objects.filter(id=int(comment['userId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'type',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'deviceToken',
                                                                'lastLoginDate',
                                                                'authToken')
                comment['creator'] = list(commentUsers)[0]
                allCommentsWithCreator.append(comment)
            allCommentsWithCreator = sorted(allCommentsWithCreator, key=lambda x: x['updationDate'], reverse=True)
            post['comments'] = list(allCommentsWithCreator)
            likes = Like.objects.filter(postId=int(post['id']))
            post['total_likes'] = likes.count()
            for tagUsersIdObj in tagUsersIds:
                newTag = Tag(tagText=str(""),
                    userId=str(tagUsersIdObj['id']),
                    postId=str(post['id']))
                newTag.save()
            tags = Tag.objects.filter(postId=int(post['id'])).values('id',
                                                            'userId',
                                                            'postId')
            allTaggedUsers = []
            for tag in tags:
                taggedUsers = User.objects.filter(id=int(tag['userId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'type',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'lastLoginDate')
                if taggedUsers.count() > 0:
                    allTaggedUsers.append(list(taggedUsers)[0])
            post['tagged_users'] = list(allTaggedUsers)
            likes = Like.objects.filter(postId=int(post['id'])).values('userId')
            likingUsers = []
            for like in likes:
                likedUsers = User.objects.filter(id=int(like['userId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'type',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'lastLoginDate')

                if likedUsers.count() > 0:
                    likingUsers.append(list(likedUsers)[0])
            post['total_likes'] = likes.count()
            post['liking_users'] = list(likingUsers)
            return Response({"NewPost" :post})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def AddPostWithMediaWithSubjects(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            postTitle = body['post_title']
            postDescription = body['post_description']
            tagUsersIds = body['tag_users_ids']
            medias = body['medias']
            classId = body['class_id']
            subjects = body['subjects']
            users = User.objects.filter(authToken=str(auth)).values('id','type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            user = list(users)[0]
            if user['type'] == "Student":
                return Response({"Error" :"User of type "+user['type']+" do not have ability to add new Posts."})

            for media in medias:
                if media['media_link'] is None:
                    mediaData = media['media_data']
                    extension = ".jpg"
                    contentType = "image/jpeg"
                    if media['media_type'] == "video":
                        extension = ".mp4"
                        contentType = "video/mp4"
                    AWS_ACCESS_KEY = 'AKIAJBOEQASD4L7YUKFQ'
                    AWS_SECRET_KEY = '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'
                    AWS_BUCKET_NAME = 'uchoose'
                    connection = boto.s3.connect_to_region('us-east-1',
                                                            aws_access_key_id=AWS_ACCESS_KEY,
                                                            aws_secret_access_key=AWS_SECRET_KEY,
                                                            #is_secure=True,               # uncomment if you are not using ssl
                                                            calling_format = boto.s3.connection.OrdinaryCallingFormat(),
                                                            )
                    bucket = connection.get_bucket(AWS_BUCKET_NAME)
                    cloudfile = Key(bucket)
                    millis = int(round(time.time() * 1000))
                    path = "KidsApp/" + str(user['id']) + str(millis) + extension
                    cloudfile.key = path
                    cloudfile.set_contents_from_string(base64.b64decode(mediaData))
                    cloudfile.set_metadata('Content-Type', contentType)
                    cloudfile.set_acl('public-read')
                    media['media_link'] = "https://s3.amazonaws.com/uchoose/" + path

            newPost = Post(postTitle=str(postTitle),
                    postDescription=str(postDescription),
                    creatorId=int(user['id']),
                    classId=int(classId))
            newPost.save()
            for media in medias:
                mediaLink = media['media_link']
                if "https://s3.amazonaws.com/uchoose/" not in mediaLink:
                    mediaLink = "https://uchoose-development.s3.amazonaws.com/" + mediaLink




                mediaType = media['media_type']
                newMedia= Media(mediaLink=str(mediaLink),
                        mediaType=str(mediaType),
                        postId=int(newPost.id))
                newMedia.save()
            posts = Post.objects.filter(id=int(newPost.id)).values('id',
                                                            'postTitle',
                                                            'postDescription',
                                                            'creatorId',
                                                            'creationDate',
                                                            'updationDate',
                                                            'classId')
            post = list(posts)[0]
            users = User.objects.filter(id=int(post['creatorId'])).values('id',
                                                            'userName',
                                                            'email',
                                                            'type',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'lastLoginDate',
                                                            'authToken')
            post['creator'] = list(users)[0]

            medias = Media.objects.filter(postId=int(post['id'])).values('id',
                                                            'mediaLink',
                                                            'mediaType',
                                                            'creationDate')
            post['medias'] = list(medias)
            comments = Comment.objects.filter(postId=int(post['id']),isActive=True).values('id',
                                                            'commentTitle',
                                                            'commentMessage',
                                                            'creationDate',
                                                            'updationDate',
                                                            'userId')
            post['total_comments'] = comments.count()
            allCommentsWithCreator = []
            for comment in comments:
                commentUsers = User.objects.filter(id=int(comment['userId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'type',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'deviceToken',
                                                                'lastLoginDate',
                                                                'authToken')
                comment['creator'] = list(commentUsers)[0]
                allCommentsWithCreator.append(comment)
            allCommentsWithCreator = sorted(allCommentsWithCreator, key=lambda x: x['updationDate'], reverse=True)
            post['comments'] = list(allCommentsWithCreator)
            likes = Like.objects.filter(postId=int(post['id']))
            post['total_likes'] = likes.count()
            for tagUsersIdObj in tagUsersIds:
                newTag = Tag(tagText=str(""),
                    userId=str(tagUsersIdObj['id']),
                    postId=str(post['id']))
                newTag.save()
            tags = Tag.objects.filter(postId=int(post['id'])).values('id',
                                                            'userId',
                                                            'postId')
            allTaggedUsers = []
            for tag in tags:
                taggedUsers = User.objects.filter(id=int(tag['userId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'type',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'lastLoginDate')
                if taggedUsers.count() > 0:
                    taggedUser = list(taggedUsers)[0]
                    if taggedUser['type'] == "Student":
                        studentParents = StudentParent.objects.filter(studentId=int(taggedUser['id'])).values('id',
                                                                        'studentId',
                                                                        'parentId')
                        for studentParent in studentParents:
                            parents = User.objects.filter(id=int(studentParent['parentId'])).values('id',
                                                                            'userName',
                                                                            'email',
                                                                            'type',
                                                                            'age',
                                                                            'phone',
                                                                            'gender',
                                                                            'profilePictureURL',
                                                                            'lastLoginDate')
                            allTaggedUsers.extend(list(parents))
                    else:
                        allTaggedUsers.append(list(taggedUsers)[0])



                    #allTaggedUsers.append(list(taggedUsers)[0])
            post['tagged_users'] = list(allTaggedUsers)
            likes = Like.objects.filter(postId=int(post['id'])).values('userId')
            likingUsers = []
            for like in likes:
                likedUsers = User.objects.filter(id=int(like['userId'])).values('id',
                                                                'userName',
                                                                'email',
                                                                'type',
                                                                'age',
                                                                'phone',
                                                                'gender',
                                                                'profilePictureURL',
                                                                'lastLoginDate')

                if likedUsers.count() > 0:
                    likingUsers.append(list(likedUsers)[0])



            for subject in subjects:
                subjectId = subject['subject_id']
                subjectPosts = SubjectPost(postId=int(post['id']),subjectId=int(subjectId))
                subjectPosts.save()


            allSubjects = []
            subjectPosts = SubjectPost.objects.filter(postId=int(post['id'])).values('id',
                                                            'subjectId')
            for subjectPost in subjectPosts:
                subjects = Subject.objects.filter(id=int(subjectPost['subjectId'])).values('id',
                                                                'subjectName')
                allSubjects.extend(subjects)
            post['subjects'] = allSubjects
            post['total_likes'] = likes.count()
            post['liking_users'] = list(likingUsers)

            pushTitle = post['creator']['userName'] + "has tagged you in a post."
            #for taggedUser in post['tagged_users']:
            #
            pushIds = []
            for taggedUser in post['tagged_users']:
                if int(taggedUser['id']) not in pushIds:
                    pushIds.append(int(taggedUser['id']))
            if len(pushIds) > 0:
                statusFCM = fcm_send_notification(pushIds, int(post['id']) ,title=str(pushTitle), body="")
                send_apns_notification(pushIds, int(post['id']), str(pushTitle))
            return Response({"NewPost" :post})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})











@api_view(['POST', ])
def UploadFileToS3(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            imageData = body['image_data']
            mediaType = body['media_type']
            extension = ".jpg"
            contentType = "image/jpeg"
            if mediaType == "video":
                extension = ".mp4"
                contentType = "video/mp4"
            users = User.objects.filter(authToken=str(auth)).values('id','type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            user = list(users)[0]
            AWS_ACCESS_KEY = 'AKIAJBOEQASD4L7YUKFQ'
            AWS_SECRET_KEY = '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'
            AWS_BUCKET_NAME = 'uchoose'
            connection = boto.s3.connect_to_region('us-east-1',
                                                    aws_access_key_id=AWS_ACCESS_KEY,
                                                    aws_secret_access_key=AWS_SECRET_KEY,
                                                    #is_secure=True,               # uncomment if you are not using ssl
                                                    calling_format = boto.s3.connection.OrdinaryCallingFormat(),
                                                    )
            bucket = connection.get_bucket(AWS_BUCKET_NAME)
            cloudfile = Key(bucket)
            millis = int(round(time.time() * 1000))
            path = "KidsApp/" + str(user['id']) + str(millis) + extension
            cloudfile.key = path
            cloudfile.set_contents_from_string(base64.b64decode(imageData))
            cloudfile.set_metadata('Content-Type', contentType)
            cloudfile.set_acl('public-read')
            return Response({"Uploaded" :"https://s3.amazonaws.com/uchoose/" + path})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def RemovePostFromTimeline(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            postID = int(body['post_id'])
            users = User.objects.filter(authToken=str(auth)).values('id','type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            user = list(users)[0]


            posts = Post.objects.filter(id=int(postID))
            if posts.count() == 0:
                return Response({"Error" :"No Post Found with specified Id."})
            post = list(posts)[0]

            post.isActive = False
            post.save()

            return Response({"Status": "Post has been removed."})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})




@api_view(['POST', ])
def EditPost(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            postID = int(body['post_id'])
            users = User.objects.filter(authToken=str(auth)).values('id',
                                                            'userName',
                                                            'email',
                                                            'type',
                                                            'age',
                                                            'phone',
                                                            'gender',
                                                            'profilePictureURL',
                                                            'deviceToken',
                                                            'lastLoginDate',
                                                            'authToken')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            user = list(users)[0]
            posts = Post.objects.filter(id=int(postID),isActive=True).values('id',
                                                        'postTitle',
                                                        'postDescription',
                                                        'creatorId',
                                                        'creationDate',
                                                        'updationDate',
                                                        'classId')
            if posts.count() == 0:
                return Response({"Error" :"No Post Found with specified Id."})
            for i in body["PostData"]:
                if i == "title":
                    posts = Post.objects.filter(id=int(postID),isActive=True)
                    for j in posts:
                        j.postTitle = body["PostData"]['title']
                        j.save()
                elif i == "description":
                    posts = Post.objects.filter(id=int(postID),isActive=True)
                    for j in posts:
                        j.postDescription = body["PostData"]['description']
                        j.save()
                elif i == "removed_media_ids":
                    removedMediaIds = body["PostData"]['removed_media_ids']
                    for removedMediaIdObj in removedMediaIds:
                        medias = Media.objects.filter(id=int(removedMediaIdObj['id']))
                        for j in medias:
                            j.delete()
                elif i == "new_medias":
                    medias = body["PostData"]['new_medias']
                    for media in medias:
                        if not('media_link' in media):
                            mediaData = media['media_data']
                            extension = ".jpg"
                            contentType = "image/jpeg"
                            if media['media_type'] == "video":
                                extension = ".mp4"
                                contentType = "video/mp4"
                            AWS_ACCESS_KEY = 'AKIAJBOEQASD4L7YUKFQ'
                            AWS_SECRET_KEY = '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'
                            AWS_BUCKET_NAME = 'uchoose'
                            connection = boto.s3.connect_to_region('us-east-1',
                                                                    aws_access_key_id=AWS_ACCESS_KEY,
                                                                    aws_secret_access_key=AWS_SECRET_KEY,
                                                                    #is_secure=True,               # uncomment if you are not using ssl
                                                                    calling_format = boto.s3.connection.OrdinaryCallingFormat(),
                                                                    )
                            bucket = connection.get_bucket(AWS_BUCKET_NAME)
                            cloudfile = Key(bucket)
                            millis = int(round(time.time() * 1000))
                            path = "KidsApp/" + str(user['id']) + str(millis) + extension
                            cloudfile.key = path
                            cloudfile.set_contents_from_string(base64.b64decode(mediaData))
                            cloudfile.set_metadata('Content-Type', contentType)
                            cloudfile.set_acl('public-read')
                            media['media_link'] = "https://s3.amazonaws.com/uchoose/" + path

                        mediaLink = media['media_link']
                        mediaType = media['media_type']
                        newMedia= Media(mediaLink=str(mediaLink),
                                mediaType=str(mediaType),
                                postId=int(postID))
                        newMedia.save()

            return Response({"Status": "Post Updated."})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def RemoveCommentFromPost(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            commentID = int(body['comment_id'])
            users = User.objects.filter(authToken=str(auth)).values('id','type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            user = list(users)[0]
            comments = Comment.objects.filter(id=int(commentID))
            if comments.count() == 0:
                return Response({"Error" :"No Comment Found with specified Id."})
            comment = list(comments)[0]
            comment.isActive = False
            comment.save()

            return Response({"Status": "Comment has been removed."})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def EditCommentFromPost(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            commentID = int(body['comment_id'])
            commentTitle = body['comment_title']
            users = User.objects.filter(authToken=str(auth)).values('id','type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            user = list(users)[0]
            comments = Comment.objects.filter(id=int(commentID))
            if comments.count() == 0:
                return Response({"Error" :"No Comment Found with specified Id."})
            comment = list(comments)[0]
            comment.commentTitle = commentTitle
            comment.save()

            return Response({"Status": "Comment has been updated."})
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})






@api_view(['POST', ])
def AddRemoveLikeForPost(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            postId = int(body['post_id'])
            users = User.objects.filter(authToken=str(auth)).values('id','type')
            if users.count() == 0:
                return Response({"Error" :"Authorization Token is not correct."})
            user = list(users)[0]
            if Like.objects.filter(userId=int(user['id']), postId=int(postId)).count() == 0:
                newLike = Like(userId=int(user['id']),
                            postId=int(postId))
                newLike.save()
            else :
                likes = Like.objects.filter(postId=int(postId), userId=int(user['id'])).delete()
            likes = Like.objects.filter(postId=int(postId)).values('userId','postId')

            return Response({"total_likes": len(likes) })
        except KeyError as e:
            return Response({"Error": "KeyError "+str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError "+str(e)})
        except Exception as e:
            return Response({"Error": str(e)})
